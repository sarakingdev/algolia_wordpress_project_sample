<?php
$params = array(
    'keywords' => '',
    'doc_date_min' => '',
    'doc_date_max' => '',
    'posted_date_min' => '',
    'posted_date_max' => '',
    'source' => '',
    'exclude_source' => '',
    'document_type' => '',
    'exclude_document_type' => '',
    'category' => '',
    'topic' => '',
    'exclude_category' => '',
    'collection' => '',
    'exclude_collection' => '',
    'key' => 0,
    'index' => 'dev_pns'
);
// are we either passing data from a bookmarklet or have we posted a form
if ((isset($_GET['p']))&&(strlen($_GET['p'])>0)){
    if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php')) {
        require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');
    }
    require $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
    global $wpdb;
    global $algolia;
    $client = Algolia\AlgoliaSearch\SearchClient::create(
      'xxxxxx',
      'xxxxxx'
    );
    $index = $client->initIndex('dev_pns_bookmarklets');
    $p = $_GET['p'];
    $get_data = $index->search($p, [
        'hitsPerPage' => 1,
        'attributesToHighlight' => null
    ]);
    if($get_data['nbHits']>0){
        $data = $get_data['hits'][0]['data'];
        $data = unserialize($data);
    }
} else {
    $data = array(
        'doc_date_method' => 'all',
        'doc_date_min' => '',
        'doc_date_max' => '',
        'posted_date_method' => 'all',
        'posted_date_min' => '',
        'posted_date_max' => '',
        'keywords' => '',
        'topic' => '',
        'source' => '',
        'exclude_source' => array(),
        'document_type' => array(),
        'exclude_document_type' => array(),
        'category' => array(),
        'exclude_category' => array(),
        'collection' => '',
        'exclude_collection' => array(),
        'key' => 0
    );
    if ((isset($_REQUEST))&&(!empty($_REQUEST))){
        // posted submission

        if ((isset($_REQUEST['keywords']))&&(strlen($_REQUEST['keywords'])>0)){
            $data['keywords'] = $_REQUEST['keywords'];
        }
        if ((isset($_REQUEST['category']))&&(strlen($_REQUEST['category'])>0)){
            $posted_categories = array();
            $posted_category = $_REQUEST['category'];
            $get_category_id = $wpdb->get_results("select term_id from wp_terms where slug = '$posted_category'");
            if (!empty($get_category_id)){
                $posted_categories[] = $get_category_id[0]->term_id;
                $data['category'] = $posted_categories;
            } else {
                $data['category'] = array();
            } 
        }
        if ((isset($_REQUEST['document_type']))&&(strlen($_REQUEST['document_type'])>0)){
            $posted_document_types = array();
            $posted_document_type = $_REQUEST['document_type'];
            $get_document_type_id = $wpdb->get_results("select term_id from wp_terms where slug = '$posted_document_type'");
            if (!empty($get_document_type_id)){
                $posted_document_types[] = $get_document_type_id[0]->term_id;
                $data['document_type'] = $posted_document_types;
            } else {
                $data['document_type'] = array();
            } 
        }
    }
}
if ($data){

    $active_filters = array();

    // -------------------------------------------------------------------------------------------
    // keywords
    if ((isset($data['keywords']))&&(strlen($data['keywords'])>0)){
        $params['keywords'] = $data['keywords'];
        $active_filters[] = '<a href="javascript:void(0);" data-filter="keywords" data-filter-type="keywords">Keywords: '.removeAllSlashes($params['keywords']).'</a>';
    }
    if ((isset($data['topic']))&&(strlen($data['topic'])>0)){
        //$params['keywords'] = $data['keywords'];
        $active_filters[] = '<a href="javascript:void(0);" data-filter="topic" data-filter-type="topic">Topic: '.removeAllSlashes($params['topic']).'</a>';
    }
    if ((isset($data['collection']))&&(strlen($data['collection'])>0)){
        //$params['keywords'] = $data['keywords'];
        $active_filters[] = '<a href="javascript:void(0);" data-filter="collection" data-filter-type="collection">Collection: '.removeAllSlashes($params['collection']).'</a>';
    }
    if ((isset($data['source']))&&(strlen($data['source'])>0)){
        //$params['keywords'] = $data['keywords'];
        $active_filters[] = '<a href="javascript:void(0);" data-filter="source" data-filter-type="source">Source: '.removeAllSlashes($params['source']).'</a>';
    }

    // -------------------------------------------------------------------------------------------
    // document and posted dates

    if ($data['doc_date_method']!='all'){
        $params['set_doc_date'] = true;
        if ($data['doc_date_method']=='past24'){
            // last 24 hours
            $now = date('U');
            $past24 = $now - (60*60*24);
            $params['doc_date_min'] = date('n/j/Y', $past24);
            $params['doc_date_max'] = date('n/j/Y', $now);
        } else if ($data['doc_date_method']=='past48'){
            // past 48 hours
            $now = date('U');
            $past48 = $now - (60*60*48);
            $params['doc_date_min'] = date('n/j/Y', $past48);
            $params['doc_date_max'] = date('n/j/Y', $now);
        } else if ($data['doc_date_method']=='specific_date') {
            $params['doc_date_min'] = date('n/j/Y', strtotime($data['doc_date_min']));
            $params['doc_date_max'] = date('n/j/Y', strtotime($data['doc_date_max']));
        }
        if ($params['doc_date_min'] == $params['doc_date_max']){
            $active_filters[] = '<a href="javascript:void(0);" data-filter="doc_date_min" data-filter-type="doc_date_min">Document Date: ' . $params['doc_date_min'] . '</a>';
        } else {
            $active_filters[] = '<a href="javascript:void(0);" data-filter="doc_date_min" data-filter-type="doc_date_min">Document Start Date: ' . $params['doc_date_min'] . '</a>';
            $active_filters[] = '<a href="javascript:void(0);" data-filter="doc_date_max" data-filter-type="doc_date_max">Document End Date: ' . $params['doc_date_max'] . '</a>';
        }
    } else {
        $now = date('U');
        $params['doc_date_min'] = date('n/j/Y', strtotime('01/01/1000'));
        $params['doc_date_max'] = date('n/j/Y', strtotime('12/31/2999'));
        $params['set_doc_date'] = false;
    }
    if ($data['posted_date_method']!='all'){
        $params['set_posted_date'] = true;
        if ($data['posted_date_method']=='past24'){
            // last 24 hours
            $now = date('U');
            $past24 = $now - (60*60*24);
            $params['posted_date_min'] = date('n/j/Y', $past24);
            $params['posted_date_max'] = date('n/j/Y', $now);
        } else if ($data['posted_date_method']=='past48'){
            // past 48 hours
            $now = date('U');
            $past48 = $now - (60*60*48);
            $params['posted_date_min'] = date('n/j/Y', $past48);
            $params['posted_date_max'] = date('n/j/Y', $now);
        } else if ($data['posted_date_method']=='specific_date') {
            $params['posted_date_min'] = date('n/j/Y', strtotime($data['posted_date_min']));
            $params['posted_date_max'] = date('n/j/Y', strtotime($data['posted_date_max']));
        }
        if ($params['posted_date_min'] == $params['posted_date_max']){
            $active_filters[] = '<a href="javascript:void(0);" data-filter="doc_date_min" data-filter-type="posted_date_min">Document Date: ' . $params['posted_date_min'] . '</a>';
        } else {
            $active_filters[] = '<a href="javascript:void(0);" data-filter="posted_date_min" data-filter-type="posted_date_min">Posted Start Date: ' . $params['posted_date_min'] . '</a>';
            $active_filters[] = '<a href="javascript:void(0);" data-filter="posted_date_max" data-filter-type="posted_date_max">Posted End Date: ' . $params['posted_date_max'] . '</a>';
        }
    } else {
        $now = date('U');
        $params['posted_date_min'] = date('n/j/Y', strtotime('01/01/1000'));
        $params['posted_date_max'] = date('n/j/Y', strtotime('12/31/2999'));
        $params['set_posted_date'] = false;
    }

    // -------------------------------------------------------------------------------------------
    // doc types

    if (!empty($data['document_type'])){
        $doc_types = $data['document_type'];
        // we now have to check to see if there are any children
        foreach ($doc_types as $key => $doc_type){
            $doc_type_children = get_term_children( $doc_type, 'documenttype' );
            if (!empty($doc_type_children)){
                $doc_types = array_merge($doc_types, $doc_type_children);
            }
        }
        if ((isset($data['exclude_document_type']))&&(is_array($data['exclude_document_type']))&&(count($data['exclude_document_type'])>0)){
            $exclude_doc_types = $data['exclude_document_type'];
            // now, do the same thing as the include sources - get the children
            foreach ($exclude_doc_types as $key => $doc_type){
                $doc_type_children = get_term_children( $doc_type, 'documenttype' );
                if (!empty($doc_type_children)){
                    $exclude_doc_types = array_merge($exclude_doc_types, $doc_type_children);
                }
            }
            // now we can remove them from the sources array
            foreach ($exclude_doc_types as $key => $exclude_doc_type){
                $k = array_search($exclude_doc_type, $doc_types);
                if ($k!==false){
                    unset($doc_types[$k]);
                }
            }
        }
        if (!empty($doc_types)){
            // build the query string to get the slugs
            $i = 0;
            $where = '';
            foreach ($doc_types as $key => $doc_type){
                if ($i>0){
                    $where .= ' or ';
                }
                $where .= "(term_id = '".$doc_type."')";
                $i++;
            }
            $query = "select name, slug from wp_terms where $where";
            $doc_types = $wpdb->get_results($query);
            $arr_doc_types = array();
            foreach ($doc_types as $k => $v){
                $arr_doc_types[] = $v->slug;
                $active_filters[] = '<a href="javascript:void(0);" data-filter="document_type-filter-'.$v->slug.'" data-filter-type="document_type">Document Type: ' . $v->name . '</a>';
            }
            $params['document_type'] = implode('|', $arr_doc_types);
        }
    } else {
        $params['document_type'] = '';
    }
    unset($params['exclude_document_type']);

    // -------------------------------------------------------------------------------------------
    // categories

    if (!empty($data['category'])){
        $categories = $data['category'];
        // we now have to check to see if there are any children
        foreach ($categories as $key => $category){
            $category_children = get_term_children( $category, 'newscategory' );
            if (!empty($category_children)){
                $categories = array_merge($categories, $category_children);
            }
        }
        if ((isset($data['exclude_category']))&&(is_array($data['exclude_category']))&&(count($data['exclude_category'])>0)){
            $exclude_categories = $data['exclude_category'];
            // now, do the same thing as the include sources - get the children
            foreach ($exclude_categories as $key => $category){
                $category_children = get_term_children( $category, 'newscategory' );
                if (!empty($category_children)){
                    $exclude_categories = array_merge($exclude_categories, $category_children);
                }
            }
            // now we can remove them from the sources array
            foreach ($exclude_categories as $key => $exclude_category){
                $k = array_search($exclude_category, $categories);
                if ($k!==false){
                    unset($categories[$k]);
                }
            }
        }
        if (!empty($categories)){
            // build the query string to get the slugs
            $i = 0;
            $where = '';
            foreach ($categories as $key => $category){
                if ($i>0){
                    $where .= ' or ';
                }
                $where .= "(term_id = '".$category."')";
                $i++;
            }
            $query = "select name, slug from wp_terms where $where";
            $categories = $wpdb->get_results($query);
            $arr_categories = array();
            foreach ($categories as $k => $v){
                $arr_categories[] = $v->slug;
                $active_filters[] = '<a href="javascript:void(0);" data-filter="category-filter-'.$v->slug.'" data-filter-type="category">Category: ' . $v->name . '</a>';
            }
            $params['category'] = implode('|', $arr_categories);
        }
    } else {
        $params['category'] = '';
    }
    unset($params['exclude_category']);

    if (isset($data['key'])){
        $params['key'] = $data['key'];
        if ($data['key']==1){
            $active_filters[] = '<a href="javascript:void(0);" data-filter="key" data-filter-type="key">Show Key Documents Only</a>';
        }
    }    
    if (isset($data['sort_order'])){
        if ($data['sort_order']=='doc_date_desc'){
            $params['index'] = 'dev_pns';
        } else if ($data['sort_order'] == 'doc_date_asc'){
            $params['index'] = 'dev_pns_sortby_document_date_asc';
        } else if ($data['sort_order'] == 'posted_date_desc'){
            $params['index'] = 'dev_pns_sortby_posted_date_desc';
        } else if ($data['sort_order'] == 'posted_date_asc'){
            $params['index'] ='dev_pns_sortby_posted_date_asc';
        }
    } else {
        $params['index'] = 'dev_pns';
    }
}

?>