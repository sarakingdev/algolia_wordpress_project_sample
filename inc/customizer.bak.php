<?php
/**
 * primarynews Theme Customizer
 *
 * @package primarynews
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function primarynews_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			'blogname',
			array(
				'selector'        => '.site-title a',
				'render_callback' => 'primarynews_customize_partial_blogname',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'blogdescription',
			array(
				'selector'        => '.site-description',
				'render_callback' => 'primarynews_customize_partial_blogdescription',
			)
		);


		// Block editor color picker - isn't appearing
		add_theme_support( 'editor-color-palette',
		    '#4F4D4A',//$black
			'#D83E35',//$red
			'#E0655E',//$salmon
			'#2B606A',//$blue
			'#7098a3',//$teal
			'#A26A00',//$lightbrown
			'#AB6800',//$brown
			'#617F4F',//$green
			'#7A7571',//$grey
		  );
	}
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'link_color', array(
	'label'      => __( 'Body Color', 'primarynews' ),
	'section'    => 'colors',
	'settings'   => 'bpm_bodycolor',
	) ) );
}
add_action( 'customize_register', 'primarynews_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function primarynews_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function primarynews_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function primarynews_customize_preview_js() {
	wp_enqueue_script( 'primarynews-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), _S_VERSION, true );
}
add_action( 'customize_preview_init', 'primarynews_customize_preview_js' );
