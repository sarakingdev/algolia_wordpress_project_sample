<?php 

// echo '<h1>PARAMETERS</h1>';

// -------------------------------------------------------------------------------------------
// sidebar - search form
// you can dynamically add or remove form fields by passing in the values in the sidebar include. 
// see template-alldocuments for an example and further information

$boolean_method = array(
    'and' => 'All of these words',
    'or' => 'Any of these words',
    'exact' => 'As a phrase'
);

$include_keywords = false; if ((isset($args['include_keywords']))&&($args['include_keywords']===true)){ $include_keywords = true; }
$include_date_range = false; if ((isset($args['include_date_range']))&&($args['include_date_range']===true)){ $include_date_range = true; }
$include_source = false; if ((isset($args['include_source']))&&($args['include_source']===true)){ 
    $include_source = true; 
    //$sources = get_search_sources();
}
$include_document_type = false; if ((isset($args['include_document_type']))&&($args['include_document_type']===true)){ 
    $include_document_type = true; 
    $document_types = get_search_document_types();
}
//$include_tag = false; if (in_array('tag', $args)){ $include_tag = true; }
$include_category = false; if ((isset($args['include_category']))&&($args['include_category']===true)){ 
    $include_category = true; 
    $categories = get_search_categories();
}
$include_topic = true; if ((isset($args['include_topic']))&&($args['include_topic']===true)){ 
    $include_topic = true; 
}
$include_collection = false; if ((isset($args['include_collection']))&&($args['include_collection']===true)){ 
    $include_collection = true; 
    //$collections = get_search_collections();
}
$include_sort = false; if ((isset($args['include_sort']))&&($args['include_sort']===true)){ 
    $include_sort = true; 
}

$archive = false;
if ((isset($args['archive']))&&($args['archive']===true)){
    $archive = true;
    $archive_key = $args['archive_key'];
    $archive_value = $args['archive_value'];
    $archive_name = $args['archive_name'];

}

?>

<script src="https://cdn.jsdelivr.net/algoliasearch/3/algoliasearch.min.js"></script>
<script src="https://cdn.jsdelivr.net/autocomplete.js/0/autocomplete.jquery.min.js"></script>

<aside class="doc-search">
    <!-- <p class="small-instructional">Instructional text for boolean search. Keyword search and/or, source OR, type OR</p> -->
    <form method="post" action="/all-source-documents/">
    <!-- <form method="post" action="/algolia-search-test/"> -->

        <input type="hidden" name="reset" id="reset" value="false">

        <?php 
        // -------------------------------------------------------------------------------------------
        // Keywords
        if ($include_keywords){
            if ((isset($args['keywords']))&&($args['keywords']!='')){
                $value = $args['keywords'];
            } else {
                $value = '';
            }
        ?>
        <label for="keywords" class="sm-label ">Search by Keyword</label>
        <div class="search-field-wrapper">
            <div class="keywords-wrapper">
                <span class="search-field-wrapper">
                    <input type="text" name="keywords" id="keywords" placeholder="Keywords" value="<?=removeAllSlashes($value)?>">
                    <!-- <i class="fa fa-search" aria-hidden="true"></i> -->
                </span>
                <span class="select-wrapper">
                    <select name="boolean_method" id="boolean_method">
                        <?php 
                        foreach ($boolean_method as $key => $boolean_option){
                            echo '<option value="'.$key.'">'.$boolean_option.'</option>';
                        }
                        ?>
                    </select>
                </span>
            </div>
        </div>
        <div style="height:32px;"></div>
        <?php 
        }
        // -------------------------------------------------------------------------------------------
        // Date Range
        if ($include_date_range){
            // this relies on the Litepicker library which is framework/library-agnostic javascript datepicker
            // on the front end it will show either a date range or a single date, depending on what is chosen
            // however, behind the scenes, we're always setting two dates - a min_date and a max_date, even if
            // they are the same. You can also pass a keyword, like "min_date=today" or "min_date=yesterday"
            // @@TODO: expand the ability to use keywords - maybe do some date keyword math using strtotime?
            // @@TODO: maybe change the key to 'date' rather than min_date for a keyword search (i.e. date=today)?
        ?>
        <label for="select-doc_date" class="sm-label ">Document Date/Range</label>
        <div class="date-range-wrapper">
            <div class="date-field-wrapper">
                
                <div class="date-field-inner-wrapper">
                    <input class="datepicker" type="text" name="select-doc_date-start" id="select-doc_date-start" placeholder="Start" value="">
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                </div>
                <div class="date-field-inner-wrapper">    
                    <input class="datepicker" type="text" name="select-doc_date-end" id="select-doc_date-end" placeholder="End" value="">
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                </div>
                
                <?php 
                // only force set the date if we're loading via a bookmarklet
                if ((isset($_GET['p']))&&(strlen($_GET['p'])>0)&&(isset($args['set_doc_date']))&&($args['set_doc_date']===true)){
                    if ($args['doc_date_min'] != ""){
                        echo '<input type="hidden" name="doc_date_min" id="doc_date_min" value="'.$args['doc_date_min'].'">';
                    } else {
                        echo '<input type="hidden" name="doc_date_min" id="doc_date_min" value="">';
                    }
                    if ($args['doc_date_max'] != ""){
                        echo '<input type="hidden" name="doc_date_max" id="doc_date_max" value="'.$args['doc_date_max'].'">';
                    } else {
                        echo '<input type="hidden" name="doc_date_max" id="doc_date_max" value="">';
                    }       
                } else {
                    echo '<input type="hidden" name="doc_date_min" id="doc_date_min" value="">';
                    echo '<input type="hidden" name="doc_date_max" id="doc_date_max" value="">';
                }
                ?>
            </div>
            <script>
            const startDocDate = new Litepicker({ 
                element: document.getElementById('select-doc_date-start'),
                singleMode: true,
                allowRepick:true,
                autoRefresh:true,
                format: 'MM/DD/YYYY',
                dropdowns: {
                    minYear: new Date().getFullYear() - 150,
                    //maxYear: new Date().getFullYear() + 100,
                    maxYear: new Date().getFullYear() + 2,
                    months: true,
                    years: true
                },
                setup: (picker) => {
                    picker.on('before:show', (el) => {
                        picker.setOptions({
                            singleMode: true,
                            allowRepick:true,
                            autoRefresh:true,
                            format: 'MM/DD/YYYY',
                            autoApply:false,
                            resetButton: true,
                            dropdowns: {
                                minYear: new Date().getFullYear() - 170,
                                maxYear: new Date().getFullYear() + 2,
                                months: true,
                                years: true
                            },
                            showTooltip:false
                        });
                    }),
                    picker.on('render', (ui) => {
                        // $(".litepicker .container__footer").append('<p class="small-instructional" id="date-instructions">Click once to choose a start date then again to choose an end date or double-click to choose a single day.</p>');
                    }),
                    // this checks the min and max dates and sees if they are the same
                    // depending on that comparison, we are resetting the options for the datepicker
                    // to be either a single date or a date range. Again, this is just a front end 
                    // thing, as it still sets both fields (min/max)
                    picker.on('button:apply', (date1) => {

                    //picker.on('selected', (date1, date2) => {
                        // var displayDate = '';
                        // if (JSON.stringify(date1)===JSON.stringify(date2)){
                        //     picker.setOptions({
                        //         singleMode: true,
                        //         allowRepick:true,
                        //         autoRefresh:true,
                        //         format: 'MM/DD/YYYY',
                        //     });
                        //     dt1 = date1.dateInstance;
                        //     dt1 = dt1.toLocaleString(dt1.DATE_SHORT);
                        //     dt1 = dt1.split(', ');
                        //     document.getElementById('doc_date_min').value = dt1[0];
                        //     document.getElementById('doc_date_max').value = dt1[0];
                        //     displayDate = dt1[0];
                        // } else {
                            dt1 = date1.dateInstance;
                            dt1 = dt1.toLocaleString(dt1.DATE_SHORT);
                            dt1 = dt1.split(', ');
                            document.getElementById('doc_date_min').value = dt1[0];
                            // dt2 = date2.dateInstance;
                            // dt2 = dt2.toLocaleString(dt2.DATE_SHORT);
                            // dt2 = dt2.split(', ');
                            // document.getElementById('doc_date_max').value = dt2[0];
                            // displayDate = dt1[0] + ' - ' + dt2[0];
                        // }
                        //document.getElementById('date-range').value = displayDate;
                    });
                }
            });

            const endDocDate = new Litepicker({ 
                element: document.getElementById('select-doc_date-end'),
                singleMode: true,
                allowRepick:true,
                autoRefresh:true,
                format: 'MM/DD/YYYY',
                dropdowns: {
                    minYear: new Date().getFullYear() - 150,
                    maxYear: new Date().getFullYear() + 2,
                    months: true,
                    years: true
                },
                setup: (picker) => {
                    picker.on('before:show', (el) => {
                        picker.setOptions({
                            singleMode: true,
                            allowRepick:true,
                            autoRefresh:true,
                            format: 'MM/DD/YYYY',
                            autoApply:false,
                            resetButton: true,
                            dropdowns: {
                                minYear: new Date().getFullYear() - 150,
                                maxYear: new Date().getFullYear() + 2,
                                months: true,
                                years: true
                            },
                            showTooltip:false
                        });
                    }),
                    picker.on('render', (ui) => {
                        // $(".litepicker .container__footer").append('<p class="small-instructional" id="date-instructions">Click once to choose a start date then again to choose an end date or double-click to choose a single day.</p>');
                    }),
                    // this checks the min and max dates and sees if they are the same
                    // depending on that comparison, we are resetting the options for the datepicker
                    // to be either a single date or a date range. Again, this is just a front end 
                    // thing, as it still sets both fields (min/max)
                    picker.on('button:apply', (date1) => {
                    //picker.on('selected', (date1, date2) => {
                        // var displayDate = '';
                        // if (JSON.stringify(date1)===JSON.stringify(date2)){
                        //     picker.setOptions({
                        //         singleMode: true,
                        //         allowRepick:true,
                        //         autoRefresh:true,
                        //         format: 'MM/DD/YYYY',
                        //     });
                        //     dt1 = date1.dateInstance;
                        //     dt1 = dt1.toLocaleString(dt1.DATE_SHORT);
                        //     dt1 = dt1.split(', ');
                        //     document.getElementById('doc_date_min').value = dt1[0];
                        //     document.getElementById('doc_date_max').value = dt1[0];
                        //     displayDate = dt1[0];
                        // } else {
                            dt1 = date1.dateInstance;
                            dt1 = dt1.toLocaleString(dt1.DATE_SHORT);
                            dt1 = dt1.split(', ');
                            document.getElementById('doc_date_max').value = dt1[0];
                        //     dt2 = date2.dateInstance;
                        //     dt2 = dt2.toLocaleString(dt2.DATE_SHORT);
                        //     dt2 = dt2.split(', ');
                        //     document.getElementById('doc_date_max').value = dt2[0];
                        //     displayDate = dt1[0] + ' - ' + dt2[0];
                        // }
                        //document.getElementById('date-range').value = displayDate;
                    });
                }
            });

            <?php 
            // only force set the date if we're loading via a bookmarklet
            if ((isset($_GET['p']))&&(strlen($_GET['p'])>0)&&(isset($args['set_doc_date']))&&($args['set_doc_date']===true)){
            /*
            ?>
            startDocDate.setDateRange('<?=$args['doc_date_min']?>', '<?=$args['doc_date_max']?>');
            <?php 
            */
            } ?>
            </script>

            <label for="select-posted_date" class="sm-label ">Posted Date/Range</label>
            <div class="date-field-wrapper">
                <div class="date-field-inner-wrapper">
                    <input class="datepicker" type="text" name="select-posted_date-start" id="select-posted_date-start" placeholder="Start" value="">
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                </div>
                <div class="date-field-inner-wrapper">    
                    <input class="datepicker" type="text" name="select-posted_date-end" id="select-posted_date-end" placeholder="End" value="">
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                </div>

                <?php 
                // only force set the date if we're loading via a bookmarklet
                if ((isset($_GET['p']))&&(strlen($_GET['p'])>0)&&(isset($args['set_posted_date']))&&($args['set_posted_date']===true)){
                    if ($args['posted_date_min'] != ""){
                        echo '<input type="hidden" name="posted_date_min" id="posted_date_min" value="'.$args['posted_date_min'].'">';
                    } else {
                        echo '<input type="hidden" name="posted_date_min" id="posted_date_min" value="">';
                    }
                    if ($args['posted_date_max'] != ""){
                        echo '<input type="hidden" name="posted_date_max" id="posted_date_max" value="'.$args['posted_date_max'].'">';
                    } else {
                        echo '<input type="hidden" name="posted_date_max" id="posted_date_max" value="">';
                    }       
                } else {
                    echo '<input type="hidden" name="posted_date_min" id="posted_date_min" value="">';
                    echo '<input type="hidden" name="posted_date_max" id="posted_date_max" value="">';
                }
                ?>
            </div>

            <script>
            const startPostedDate = new Litepicker({ 
                element: document.getElementById('select-posted_date-start'),
                singleMode: true,
                allowRepick:true,
                autoRefresh:true,
                format: 'MM/DD/YYYY',
                dropdowns: {
                    minYear: new Date().getFullYear() - 150,
                    maxYear: new Date().getFullYear() + 2,
                    months: true,
                    years: true
                },
                setup: (picker) => {
                    picker.on('before:show', (el) => {
                        picker.setOptions({
                            singleMode: true,
                            allowRepick:true,
                            autoRefresh:true,
                            format: 'MM/DD/YYYY',
                            autoApply:false,
                            resetButton: true,
                            showTooltip:false,
                            dropdowns: {
                                minYear: new Date().getFullYear() - 150,
                                maxYear: new Date().getFullYear() + 2,
                                months: true,
                                years: true
                            }
                        });

                    }),
                    picker.on('render', (ui) => {
                        //$(".litepicker .container__footer").append('<p class="small-instructional" id="date-instructions">Click once to choose a start date then again to choose an end date or double-click to choose a single day.</p>');
                    }),
                    // this checks the min and max dates and sees if they are the same
                    // depending on that comparison, we are resetting the options for the datepicker
                    // to be either a single date or a date range. Again, this is just a front end 
                    // thing, as it still sets both fields (min/max)
                    picker.on('button:apply', (date1) => {
                    //picker.on('selected', (date1, date2) => {
                        // var displayDate = '';
                        // if (JSON.stringify(date1)===JSON.stringify(date2)){
                        //     picker.setOptions({
                        //         singleMode: true,
                        //         allowRepick:true,
                        //         autoRefresh:true,
                        //         format: 'MM/DD/YYYY',
                        //     });
                        //     dt1 = date1.dateInstance;
                        //     dt1 = dt1.toLocaleString(dt1.DATE_SHORT);
                        //     dt1 = dt1.split(', ');
                        //     document.getElementById('posted_date_min').value = dt1[0];
                        //     document.getElementById('posted_date_max').value = dt1[0];
                        //     displayDate = dt1[0];
                        // } else {
                            dt1 = date1.dateInstance;
                            dt1 = dt1.toLocaleString(dt1.DATE_SHORT);
                            dt1 = dt1.split(', ');
                            document.getElementById('posted_date_min').value = dt1[0];
                        //     dt2 = date2.dateInstance;
                        //     dt2 = dt2.toLocaleString(dt2.DATE_SHORT);
                        //     dt2 = dt2.split(', ');
                        //     document.getElementById('posted_date_max').value = dt2[0];
                        //     displayDate = dt1[0] + ' - ' + dt2[0];
                        // }
                        //document.getElementById('date-range').value = displayDate;
                    });
                }
            });
            const endPostedDate = new Litepicker({ 
                element: document.getElementById('select-posted_date-end'),
                singleMode: true,
                allowRepick:true,
                autoRefresh:true,
                format: 'MM/DD/YYYY',
                dropdowns: {
                    minYear: new Date().getFullYear() - 150,
                    maxYear: new Date().getFullYear() + 2,
                    months: true,
                    years: true
                },
                setup: (picker) => {
                    picker.on('before:show', (el) => {
                        picker.setOptions({
                            singleMode: true,
                            allowRepick:true,
                            autoRefresh:true,
                            format: 'MM/DD/YYYY',
                            dropdowns: {
                                minYear: new Date().getFullYear() - 150,
                                maxYear: new Date().getFullYear() + 2,
                                months: true,
                                years: true
                            },
                            autoApply:false,
                            resetButton: true,
                            showTooltip:false
                        });

                    }),
                    picker.on('render', (ui) => {
                        //$(".litepicker .container__footer").append('<p class="small-instructional" id="date-instructions">Click once to choose a start date then again to choose an end date or double-click to choose a single day.</p>');
                    }),
                    // this checks the min and max dates and sees if they are the same
                    // depending on that comparison, we are resetting the options for the datepicker
                    // to be either a single date or a date range. Again, this is just a front end 
                    // thing, as it still sets both fields (min/max)
                    picker.on('button:apply', (date1, date2) => {
                    //picker.on('selected', (date1, date2) => {
                        // var displayDate = '';
                        // if (JSON.stringify(date1)===JSON.stringify(date2)){
                        //     picker.setOptions({
                        //         singleMode: true,
                        //         allowRepick:true,
                        //         autoRefresh:true,
                        //         format: 'MM/DD/YYYY',
                        //     });
                        //     dt1 = date1.dateInstance;
                        //     dt1 = dt1.toLocaleString(dt1.DATE_SHORT);
                        //     dt1 = dt1.split(', ');
                        //     document.getElementById('posted_date_min').value = dt1[0];
                        //     document.getElementById('posted_date_max').value = dt1[0];
                        //     displayDate = dt1[0];
                        // } else {
                            dt1 = date1.dateInstance;
                            dt1 = dt1.toLocaleString(dt1.DATE_SHORT);
                            dt1 = dt1.split(', ');
                            document.getElementById('posted_date_max').value = dt1[0];
                        //     dt2 = date2.dateInstance;
                        //     dt2 = dt2.toLocaleString(dt2.DATE_SHORT);
                        //     dt2 = dt2.split(', ');
                        //     document.getElementById('posted_date_max').value = dt2[0];
                        //     displayDate = dt1[0] + ' - ' + dt2[0];
                        // }
                        //document.getElementById('date-range').value = displayDate;
                    });
                }
            });
            <?php 
            // only force set the date if we're loading via a bookmarklet
            if ((isset($_GET['p']))&&(strlen($_GET['p'])>0)&&(isset($args['set_posted_date']))&&($args['set_posted_date']===true)){
            /*
            ?>
            startPostedDate.setDateRange('<?=$args['posted_date_min']?>', '<?=$args['posted_date_max']?>');
            <?php 
            */
            } ?>
            </script>
        </div>

        <?php 
        }
        ?>

        <?php 
        // -------------------------------------------------------------------------------------------
        // sources
        if ($include_source){
        ?>

        <label for="source" class="sm-label ">Search by Source</label>
        <div class="search-field-wrapper">
            <div class="autocomplete-wrapper">
                <span class="search-field-wrapper">

                    <?php 
                    $default_value = '';
                    if ((isset($args['archive']))&&($args['archive']===true)&&($args['archive_key']=='archive_source')){
                        $default_value = $args['archive_name'];
                    }
                    ?>
                    <input type="text" name="source" id="source" placeholder="Source" value="<?=$default_value?>">
                    <!-- <i class="fa fa-search" aria-hidden="true"></i> -->
                </span>
            </div>
        </div>
        <script>
        var client = algoliasearch('5ASBU0O7RW', '23516b33413280d86fa0cd97fd54e1bb');
        var index = client.initIndex('dev_sources');
        $('#source').autocomplete({ hint: false }, [{
            source: $.fn.autocomplete.sources.hits(index, { hitsPerPage: 5 }),
            displayKey: 'source',
            templates: {
                suggestion: function(suggestion) {
                    return suggestion._highlightResult.source.value;
                }
            }
        }]).on('autocomplete:selected', function(event, suggestion, dataset, context) {
            $("#source").val(suggestion.source);
        });
        </script>

        <?php 
        }
        ?>


        <?php 
        // -------------------------------------------------------------------------------------------
        // Categories
        if ($include_category){
            // singular use one - category, not categories
        ?>
        <label for="category" class="sm-label ">Category</label>
        <div class="dropdown-wrapper">
            <input type="text" name="category" id="category" placeholder="Category" class="has-modal">
            <input type="hidden" name="category_values" id="category_values">
        </div>
        <div id="category-filter-wrapper" class="filter-modal">
           <div class="flex-wrapper">
                <div class="filter-modal-header">
                    <h3>Select Category</h3>
                </div>
                <div class="filter-modal-fields-wrapper">
                    <div class="inner-flex-wrapper" id="category-filters">
                        <?php 
                        // $categories = get_search_categories();
                        if ($categories){
                            $total_count = count($categories);  // this  is the total number of elements
                            $total_per_col = ceil($total_count/2); 
                            $columns = array();
                            $col1 = array();
                            $col2 = array();
                            $current_col = 1;
                            $col_count = 0;
                            foreach ($categories as $key => $category){
                                // check to see if adding the current source to the column will exceed the per_col count
                                if (($col_count+1)>$total_per_col){
                                    // yes, it will exceed the per_col total
                                    // so we need to put it into the next col
                                    //echo $col_count . '<br>';
                                    $current_col++;
                                    $col_count = 0;
                                }
                                // add the source to the correct column
                                ${"col" . $current_col}[] = $category;
                                // update the col_count
                                $col_count = $col_count + 1;
                            }
                            // this creats an array that contains any active filter (passed via a query string)
                            // if the input slug exists in this array, we auto set it as checked
                            $active_category_filters = array();
                            if ((isset($args['category']))&&($args['category']!="")){
                                $active_category_filters = explode('|',$args['category']);
                            }
                            $columns = array($col1, $col2);
                            foreach ($columns as $column){
                                echo '<div class="col">';
                                    echo '<ul>';
                                        $filters = '';
                                        foreach ($column as $key => $category){
                                            $slug = cleanSlug($category['slug']);
                                            $filters .= '<li>';
                                                $filters .= '<label for="category-filter-'.$slug.'" class="check-label">';
                                                    $filters .= '<span class="check-wrapper"><input type="checkbox" name="'.$slug.'" id="category-filter-'.$slug.'"';
                                                    if (in_array($slug, $active_category_filters)){
                                                        $filters .= ' checked="checked"';
                                                    }
                                                    $filters .= '></span> <span class="label-wrapper">'.$category['name'] . '</span>';
                                                $filters .= '</label>';

                                                // category filters have children
                                                // @@TODO: expand other taxonomies to include children
                                                if (
                                                    (isset($category['children']))
                                                    && (is_array($category['children']))
                                                    && (count($category['children'])>0)
                                                ){
                                                    $filters .= '<ul>';
                                                    // --------------------------------------------------------------
                                                    // children
                                                    foreach ($category['children'] as $child_key => $child_category){
                                                        $child_slug = cleanSlug($child_category['slug']);
                                                        $filters .= '<li>';
                                                            $filters .= '<label for="category-filter-'.$child_slug.'"  class="check-label">';
                                                                $filters .= '<span class="check-wrapper"><input type="checkbox" name="'.$child_slug.'" id="category-filter-'.$child_slug.'"';
                                                                if (in_array($child_slug, $active_category_filters)){
                                                                    $filters .= ' checked="checked"';
                                                                }
                                                                $filters .= '></span> <span class="label-wrapper">'.$child_category['name'] . '</span>';
                                                            $filters .= '</label>';
                                                            if (
                                                                (isset($child_category['grandchildren']))
                                                                && (is_array($child_category['grandchildren']))
                                                                && (count($child_category['grandchildren'])>0)
                                                            ){
                                                                $filters .= '<ul>';
                                                                // --------------------------------------------------------------
                                                                // grandchildren
                                                                foreach ($child_category['grandchildren'] as $grandchild_key => $grandchild_category){
                                                                    $grandchild_slug = cleanSlug($grandchild_category['slug']);
                                                                    $filters .= '<li>';
                                                                        $filters .= '<label for="category-filter-'.$grandchild_slug.'"  class="check-label">';
                                                                            $filters .= '<span class="check-wrapper"><input type="checkbox" name="'.$grandchild_slug.'" id="category-filter-'.$grandchild_slug.'"';
                                                                            if (in_array($grandchild_slug, $active_category_filters)){
                                                                                $filters .= ' checked="checked"';
                                                                            }
                                                                            $filters .= '></span> <span class="label-wrapper">'.$grandchild_category['name'] . '</span>';
                                                                        $filters .= '</label>';
                                                                        // --------------------------------------------------------------
                                                                        // great-grandchildren
                                                                        if (
                                                                            (isset($grandchild_category['great_grandchildren']))
                                                                            && (is_array($grandchild_category['great_grandchildren']))
                                                                            && (count($grandchild_category['great_grandchildren'])>0)
                                                                        ){
                                                                            $filters .= '<ul>';
                                                                            foreach ($grandchild_category['great_grandchildren'] as $great_grandchild_key => $great_grandchild_category){
                                                                                $great_grandchild_slug = cleanSlug($great_grandchild_category['slug']);
                                                                                $filters .= '<li>';
                                                                                    $filters .= '<label for="category-filter-'.$great_grandchild_slug.'"  class="check-label">';
                                                                                        $filters .= '<span class="check-wrapper"><input type="checkbox" name="'.$great_grandchild_slug.'" id="category-filter-'.$great_grandchild_slug.'"';
                                                                                        if (in_array($great_grandchild_slug, $active_category_filters)){
                                                                                            $filters .= ' checked="checked"';
                                                                                        }
                                                                                        $filters .= '></span> <span class="label-wrapper">'.$great_grandchild_category['name'] . '</span>';
                                                                                    $filters .= '</label>';
                                                                                    // --------------------------------------------------------------
                                                                                    // great-great-grandchildren
                                                                                    if (
                                                                                        (isset($great_grandchild_category['great_great_grandchildren']))
                                                                                        && (is_array($great_grandchild_category['great_great_grandchildren']))
                                                                                        && (count($great_grandchild_category['great_great_grandchildren'])>0)
                                                                                    ){
                                                                                        $filters .= '<ul>';
                                                                                        foreach ($great_grandchild_category['great_great_grandchildren'] as $great_great_grandchild_key => $great_great_grandchild_category){
                                                                                            $great_great_grandchild_slug = cleanSlug($great_great_grandchild_category['slug']);
                                                                                            $filters .= '<li>';
                                                                                                $filters .= '<label for="category-filter-'.$great_great_grandchild_slug.'"  class="check-label">';
                                                                                                    $filters .= '<span class="check-wrapper"><input type="checkbox" name="'.$great_great_grandchild_slug.'" id="category-filter-'.$great_great_grandchild_slug.'"';
                                                                                                    if (in_array($great_great_grandchild_slug, $active_category_filters)){
                                                                                                        $filters .= ' checked="checked"';
                                                                                                    }
                                                                                                    $filters .= '></span> <span class="label-wrapper">'.$great_great_grandchild_category['name'] . '</span>';
                                                                                                $filters .= '</label>';
                                                                                                // --------------------------------------------------------------
                                                                                                // great-great-great-grandchildren
                                                                                                if (
                                                                                                    (isset($great_great_grandchild_category['great_great_great_grandchildren']))
                                                                                                    && (is_array($great_great_grandchild_category['great_great_great_grandchildren']))
                                                                                                    && (count($great_great_grandchild_category['great_great_great_grandchildren'])>0)
                                                                                                ){
                                                                                                    $filters .= '<ul>';
                                                                                                    foreach ($great_great_grandchild_category['great_great_great_grandchildren'] as $great_great_great_grandchild_key => $great_great_great_grandchild_category){
                                                                                                        $great_great_great_grandchild_slug = cleanSlug($great_great_great_grandchild_category['slug']);
                                                                                                        $filters .= '<li>';
                                                                                                            $filters .= '<label for="category-filter-'.$great_great_great_grandchild_slug.'"  class="check-label">';
                                                                                                                $filters .= '<span class="check-wrapper"><input type="checkbox" name="'.$great_great_great_grandchild_slug.'" id="category-filter-'.$great_great_great_grandchild_slug.'"';
                                                                                                                if (in_array($great_great_great_grandchild_slug, $active_category_filters)){
                                                                                                                    $filters .= ' checked="checked"';
                                                                                                                }
                                                                                                                $filters .= '></span> <span class="label-wrapper">'.$great_great_great_grandchild_category['name'] . '</span>';
                                                                                                            $filters .= '</label>';
                                                                                                        $filters .= '</li>';
                                                                                                    }
                                                                                                    $filters .= '</ul>';
                                                                                                }
                                                                                            $filters .= '</li>';
                                                                                        }
                                                                                        $filters .= '</ul>';
                                                                                    }
                                                                                $filters .= '</li>';
                                                                            }
                                                                            $filters .= '</ul>';
                                                                        }
                                                                    $filters .= '</li>';
                                                                }
                                                                $filters .= '</ul>';
                                                            }
                                                        $filters .= '</li>';
                                                    }
                                                    $filters .= '</ul>';
                                                }
                                            $filters .= '</li>';
                                        }
                                        echo $filters;
                                    echo '</ul>';
                                echo '</div>';
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="filter-modal-footer">
                    <input type="hidden" name="init-category-filters" id="init-category-filters" value="">
                    <a href="javascript:void(0);" class="update-filters" id="update-category-filter">Apply</a>
                    <a href="javascript:void(0);" class="select-all-filters" id="select-all-category-filter">Select All Filters</a>
                    <a href="javascript:void(0);" class="clear-filters" id="clear-category-filter">Clear Selected Filters</a>
                    <a href="javascript:void(0);" class="cancel-filters" id="cancel-category-filter">Cancel</a>
                </div>
            </div>
        </div>
        <?php 
        }
        // -------------------------------------------------------------------------------------------
        // Document Type
        if ($include_document_type){
            // please note that we are always using an underscore for 'document_type' and it is always singular!
        ?>
        <label for="document_type" class="sm-label ">Document Type</label>
        <div class="dropdown-wrapper">
            <input type="text" name="document_type" id="document_type" placeholder="Document Type" class="has-modal">
            <input type="hidden" name="document_type_values" id="document_type_values">
        </div>
        <div id="document_type-filter-wrapper" class="filter-modal">
           <div class="flex-wrapper">
                <div class="filter-modal-header">
                    <h3>Select Document Type</h3>
                </div>
                <div class="filter-modal-fields-wrapper">
                    <div class="inner-flex-wrapper" id="document_type-filters">
                        <?php 
                        // $document_types = get_search_document_types();
                        $total_count = count($document_types);  // this  is the total number of elements
                        $total_per_col = ceil($total_count/2); 
                        $columns = array();
                        $col1 = array();
                        $col2 = array();
                        $current_col = 1;
                        $col_count = 0;
                        foreach ($document_types as $key => $document_type){
                            // check to see if adding the current source to the column will exceed the per_col count
                            if (($col_count+1)>$total_per_col){
                                // yes, it will exceed the per_col total
                                // so we need to put it into the next col
                                //echo $col_count . '<br>';
                                $current_col++;
                                $col_count = 0;
                            }
                            // add the source to the correct column
                            ${"col" . $current_col}[] = $document_type;
                            // update the col_count
                            $col_count = $col_count + 1;
                        }
                        // this creats an array that contains any active filter (passed via a query string)
                        // if the input slug exists in this array, we auto set it as checked
                        $active_document_type_filters = array();
                        if ((isset($args['document_type']))&&($args['document_type']!="")){
                            $active_document_type_filters = explode('|',$args['document_type']);
                        }
                        $columns = array($col1, $col2);
                        foreach ($columns as $column){
                            echo '<div class="col">';
                                echo '<ul>';
                                    $filters = '';
                                    foreach ($column as $key => $document_type){
                                        $slug = cleanSlug($document_type['slug']);
                                        $filters .= '<li>';
                                            $filters .= '<label for="document_type-filter-'.$slug.'" class="check-label">';
                                                $filters .= '<span class="check-wrapper"><input type="checkbox" name="'.$slug.'" id="document_type-filter-'.$slug.'"';
                                                if (in_array($slug, $active_document_type_filters)){
                                                    $filters .= ' checked="checked"';
                                                }
                                                $filters .= '></span> <span class="label-wrapper">'.$document_type['name'] . '</span>';
                                            $filters .= '</label>';
                                            // document_type filters have children
                                            // @@TODO: expand other taxonomies to include children
                                            if (
                                                (isset($document_type['children']))
                                                && (is_array($document_type['children']))
                                                && (count($document_type['children'])>0)
                                            ){
                                                $filters .= '<ul>';
                                                // --------------------------------------------------------------
                                                // children
                                                foreach ($document_type['children'] as $child_key => $child_document_type){
                                                    $child_slug = cleanSlug($child_document_type['slug']);
                                                    $filters .= '<li>';
                                                        $filters .= '<label for="document_type-filter-'.$child_slug.'"  class="check-label">';
                                                            $filters .= '<span class="check-wrapper"><input type="checkbox" name="'.$child_slug.'" id="document_type-filter-'.$child_slug.'"';
                                                            if (in_array($child_slug, $active_document_type_filters)){
                                                                $filters .= ' checked="checked"';
                                                            }
                                                            $filters .= '></span> <span class="label-wrapper">'.$child_document_type['name'] . '</span>';
                                                        $filters .= '</label>';
                                                        if (
                                                            (isset($child_document_type['grandchildren']))
                                                            && (is_array($child_document_type['grandchildren']))
                                                            && (count($child_document_type['grandchildren'])>0)
                                                        ){
                                                            $filters .= '<ul>';
                                                            // --------------------------------------------------------------
                                                            // grandchildren
                                                            foreach ($child_document_type['grandchildren'] as $grandchild_key => $grandchild_document_type){
                                                                $grandchild_slug = cleanSlug($grandchild_document_type['slug']);
                                                                $filters .= '<li>';
                                                                    $filters .= '<label for="document_type-filter-'.$grandchild_slug.'"  class="check-label">';
                                                                        $filters .= '<span class="check-wrapper"><input type="checkbox" name="'.$grandchild_slug.'" id="document_type-filter-'.$grandchild_slug.'"';
                                                                        if (in_array($grandchild_slug, $active_document_type_filters)){
                                                                            $filters .= ' checked="checked"';
                                                                        }
                                                                        $filters .= '></span> <span class="label-wrapper">'.$grandchild_document_type['name'] . '</span>';
                                                                    $filters .= '</label>';
                                                                    // --------------------------------------------------------------
                                                                    // great-grandchildren
                                                                    if (
                                                                        (isset($grandchild_document_type['great_grandchildren']))
                                                                        && (is_array($grandchild_document_type['great_grandchildren']))
                                                                        && (count($grandchild_document_type['great_grandchildren'])>0)
                                                                    ){
                                                                        $filters .= '<ul>';
                                                                        foreach ($grandchild_document_type['great_grandchildren'] as $great_grandchild_key => $great_grandchild_document_type){
                                                                            $great_grandchild_slug = cleanSlug($great_grandchild_document_type['slug']);
                                                                            $filters .= '<li>';
                                                                                $filters .= '<label for="document_type-filter-'.$great_grandchild_slug.'"  class="check-label">';
                                                                                    $filters .= '<span class="check-wrapper"><input type="checkbox" name="'.$great_grandchild_slug.'" id="document_type-filter-'.$great_grandchild_slug.'"';
                                                                                    if (in_array($great_grandchild_slug, $active_document_type_filters)){
                                                                                        $filters .= ' checked="checked"';
                                                                                    }
                                                                                    $filters .= '></span> <span class="label-wrapper">'.$great_grandchild_document_type['name'] . '</span>';
                                                                                $filters .= '</label>';
                                                                                // --------------------------------------------------------------
                                                                                // great-great-grandchildren
                                                                                if (
                                                                                    (isset($great_grandchild_document_type['great_great_grandchildren']))
                                                                                    && (is_array($great_grandchild_document_type['great_great_grandchildren']))
                                                                                    && (count($great_grandchild_document_type['great_great_grandchildren'])>0)
                                                                                ){
                                                                                    $filters .= '<ul>';
                                                                                    foreach ($great_grandchild_document_type['great_great_grandchildren'] as $great_great_grandchild_key => $great_great_grandchild_document_type){
                                                                                        $great_great_grandchild_slug = cleanSlug($great_great_grandchild_document_type['slug']);
                                                                                        $filters .= '<li>';
                                                                                            $filters .= '<label for="document_type-filter-'.$great_great_grandchild_slug.'"  class="check-label">';
                                                                                                $filters .= '<span class="check-wrapper"><input type="checkbox" name="'.$great_great_grandchild_slug.'" id="document_type-filter-'.$great_great_grandchild_slug.'"';
                                                                                                if (in_array($great_great_grandchild_slug, $active_document_type_filters)){
                                                                                                    $filters .= ' checked="checked"';
                                                                                                }
                                                                                                $filters .= '></span> <span class="label-wrapper">'.$great_great_grandchild_document_type['name'] . '</span>';
                                                                                            $filters .= '</label>';
                                                                                            // --------------------------------------------------------------
                                                                                            // great-great-great-grandchildren
                                                                                            if (
                                                                                                (isset($great_great_grandchild_document_type['great_great_great_grandchildren']))
                                                                                                && (is_array($great_great_grandchild_document_type['great_great_great_grandchildren']))
                                                                                                && (count($great_great_grandchild_document_type['great_great_great_grandchildren'])>0)
                                                                                            ){
                                                                                                $filters .= '<ul>';
                                                                                                foreach ($great_great_grandchild_document_type['great_great_great_grandchildren'] as $great_great_great_grandchild_key => $great_great_great_grandchild_document_type){
                                                                                                    $great_great_great_grandchild_slug = cleanSlug($great_great_great_grandchild_document_type['slug']);
                                                                                                    $filters .= '<li>';
                                                                                                        $filters .= '<label for="document_type-filter-'.$great_great_great_grandchild_slug.'"  class="check-label">';
                                                                                                            $filters .= '<span class="check-wrapper"><input type="checkbox" name="'.$great_great_great_grandchild_slug.'" id="document_type-filter-'.$great_great_great_grandchild_slug.'"';
                                                                                                            if (in_array($great_great_great_grandchild_slug, $active_document_type_filters)){
                                                                                                                $filters .= ' checked="checked"';
                                                                                                            }
                                                                                                            $filters .= '></span> <span class="label-wrapper">'.$great_great_great_grandchild_document_type['name'] . '</span>';
                                                                                                        $filters .= '</label>';
                                                                                                    $filters .= '</li>';
                                                                                                }
                                                                                                $filters .= '</ul>';
                                                                                            }
                                                                                        $filters .= '</li>';
                                                                                    }
                                                                                    $filters .= '</ul>';
                                                                                }
                                                                            $filters .= '</li>';
                                                                        }
                                                                        $filters .= '</ul>';
                                                                    }
                                                                $filters .= '</li>';
                                                            }
                                                            $filters .= '</ul>';
                                                        }
                                                    $filters .= '</li>';
                                                }
                                                $filters .= '</ul>';
                                            }
                                        $filters .= '</li>';
                                    }
                                    echo $filters;
                                echo '</ul>';
                            echo '</div>';
                        }
                        ?>
                    </div>
                </div>
                <div class="filter-modal-footer">
                    <input type="hidden" name="init-document_type-filters" id="init-document_type-filters" value="">
                    <a href="javascript:void(0);" class="update-filters" id="update-document_type-filter">Apply</a>
                    <a href="javascript:void(0);" class="select-all-filters" id="select-all-document_type-filter">Select All Filters</a>
                    <a href="javascript:void(0);" class="clear-filters" id="clear-document_type-filter">Clear Selected Filters</a>
                    <a href="javascript:void(0);" class="cancel-filters" id="cancel-document_type-filter">Cancel</a>
                </div>
            </div>
        </div>

        <?php 
        }
        // -------------------------------------------------------------------------------------------
        // Search by Topic --> now called Tag ferko 01122023 per steve
        if ($include_topic){
        ?>
        <label for="topics" class="sm-label ">Search by Tag</label>
        <div class="search-field-wrapper">
            <div class="autocomplete-wrapper">
                <span class="search-field-wrapper">

                    <?php 
                    $default_value = '';
                    if ((isset($args['archive']))&&($args['archive']===true)&&($args['archive_key']=='archive_topic')){
                        $default_value = $args['archive_name'];
                    }
                    ?>
                    <input type="text" name="topics" id="topics" placeholder="Tags" value="<?=$default_value?>">
                    <!-- <i class="fa fa-search" aria-hidden="true"></i> -->
                </span>
            </div>
        </div>
        <script>
        var client = algoliasearch('5ASBU0O7RW', '23516b33413280d86fa0cd97fd54e1bb');
        var index = client.initIndex('dev_topics');
        $('#topics').autocomplete({ hint: false }, [{
            source: $.fn.autocomplete.sources.hits(index, { hitsPerPage: 5 }),
            displayKey: 'topic',
            templates: {
                suggestion: function(suggestion) {
                    return suggestion._highlightResult.topic.value;
                }
            }
        }]).on('autocomplete:selected', function(event, suggestion, dataset, context) {
            $("#topics").val(suggestion.topic);
        });
        </script>
        <?php 
        }
        // -------------------------------------------------------------------------------------------
        // Collections
        if ($include_collection){
            //debug($args);
            // singular use only - collection, not collections
        ?>

        <label for="collections" class="sm-label ">Search by Collection</label>
        <div class="search-field-wrapper">
            <div class="autocomplete-wrapper">
                <span class="search-field-wrapper">

                    <?php 
                    $default_value = '';
                    if ((isset($args['archive']))&&($args['archive']===true)&&($args['archive_key']=='archive_collection')){
                        $default_value = $args['archive_name'];
                    }
                    ?>
                    <input type="text" name="collections" id="collections" placeholder="Collection" value="<?=$default_value?>">
                    <!-- <i class="fa fa-search" aria-hidden="true"></i> -->
                </span>
            </div>
        </div>
        <script>
        var client = algoliasearch('5ASBU0O7RW', '23516b33413280d86fa0cd97fd54e1bb');
        var index = client.initIndex('dev_collections');
        $('#collections').autocomplete({ hint: false }, [{
            source: $.fn.autocomplete.sources.hits(index, { hitsPerPage: 5 }),
            displayKey: 'collection',
            templates: {
                suggestion: function(suggestion) {
                    return suggestion._highlightResult.collection.value;
                }
            }
        }]).on('autocomplete:selected', function(event, suggestion, dataset, context) {
            $("#collections").val(suggestion.collection);
        });
        </script>

        <?php 
        }
        ?>

        <div class="checkbox-wrapper">
            <label for="key">
                <input type="checkbox" value="1" name="key" id="key"<?php if ((isset($args['key']))&&($args['key']=='1')){ echo ' checked="checked"'; } ?>> Show Key Documents Only
            </label>
        </div>

        <div class="radio-group">
            <label for="doc_date_desc">
                <input type="radio" name="sort_order" id="doc_date_desc" value="doc_date_desc" <?php if ($args['index']=='dev_pns'){ echo 'checked="checked"'; } ?>> Doc Date: Recent -> Oldest
            </label>
            <label for="doc_date_asc">
                <input type="radio" name="sort_order" id="doc_date_asc" value="doc_date_asc" <?php if ($args['index']=='dev_pns_sortby_document_date_asc'){ echo 'checked="checked"'; } ?>> Doc Date: Oldest -> Recent
            </label>
            <label for="posted_date_desc">
                <input type="radio" name="sort_order" id="posted_date_desc" value="posted_date_desc" <?php if ($args['index']=='dev_pns_sortby_posted_date_desc'){ echo 'checked="checked"'; } ?>> Posted Date: Recent -> Oldest
            </label>
            <label for="posted_date_asc">
                <input type="radio" name="sort_order" id="posted_date_asc" value="posted_date_asc" <?php if ($args['index']=='dev_pns_sortby_posted_date_asc'){ echo 'checked="checked"'; } ?>> Posted Date: Oldest -> Recent
            </label>
        </div>

        <input type="hidden" name="pg" id="pg" value="1"> 
        <input type="hidden" id="index" name="index" value="dev_pns">
        <button class="button" type="submit" id="search-submit">Search <span></span></button>

        <p class="clear-all-wrapper"><a href="javascript:void(0);" id="clear-all-filters">Clear all filters</a></p>

    </form>
    <div class="info">
        <p>This database contains official news releases, reports, press briefings and other documents from hundreds of primary sources including over 200 federal government agencies and subagencies.</p>
    </div>
</aside>

<script>
var build_filter_string = function(){
    filter_strings = [];
    var pg = $("#pg").val();
    filter_strings.push('pg='+pg);
    // keywords 
    if ($("#boolean_method").val()!=""){
        
        //alert($("#boolean_method").val());
        filter_strings.push('boolean_method='+$("#boolean_method").val());

        //filter_strings.push('keywords='+encodeURIComponent($("#keywords").val()));
    }
    if ($("#keywords").val()!=""){
        filter_strings.push('keywords='+encodeURIComponent($("#keywords").val()));
    }
    // dates
    if ($("#doc_date_min").val()!=""){
        filter_strings.push('doc_date_min='+$("#doc_date_min").val());
    } else {
        filter_strings.push('doc_date_min=0');
    }
    if ($("#doc_date_max").val()!=""){
        filter_strings.push('doc_date_max='+$("#doc_date_max").val());
    }
    if ($("#posted_date_min").val()!=""){
        filter_strings.push('posted_date_min='+$("#posted_date_min").val());
    } else {
        filter_strings.push('posted_date_min=0');
    }
    if ($("#posted_date_max").val()!=""){
        filter_strings.push('posted_date_max='+$("#posted_date_max").val());
    }

    <?php if (($archive===true)&&($archive_key=='archive_topic')){ ?>
        filter_strings.push('archive=true');
        filter_strings.push('archive_key=archive_topic');
        filter_strings.push('topics=<?=rawurlencode($archive_name)?>');
    <?php } else { ?>
    if ($("#topics").val()!=""){
        filter_strings.push('topics='+encodeURIComponent($("#topics").val()));
    }
    <?php } ?>

    <?php if (($archive===true)&&($archive_key=='archive_source')){ ?>
        filter_strings.push('archive=true');
        filter_strings.push('archive_key=archive_source');
        filter_strings.push('source=<?=rawurlencode($archive_name)?>');
    <?php } else { ?>
    if ($("#source").val()!=""){
        filter_strings.push('source='+encodeURIComponent($("#source").val()));
    }
    <?php } ?>

    // document type
    var selected = [];
    $('#document_type-filters input:checked').each(function() {
        selected.push($(this).attr('name'));
    });
    if (selected.length>0){
        var filter_string = 'document_type=';
        var j = 0;
        for (i = 0; i < selected.length; i++){
            if (j > 0){
                filter_string += '|';
            }
            filter_string += selected[i];
            j++;
        }
        filter_strings.push(filter_string);
    }
    // categories
    var selected = [];
    <?php if (($archive===true)&&($archive_key=='archive_category')){ ?>
        selected.push("<?=$archive_value?>");
        //alert("CATEGORY ARCHIVE: <?=$archive_value?>");
        filter_strings.push('archive=true');
        filter_strings.push('archive_key=archive_category');
    <?php } else { ?>
        $('#category-filters input:checked').each(function() {
            selected.push($(this).attr('name'));
        });
    <?php } ?>
    if (selected.length>0){
        var filter_string = 'category=';
        var j = 0;
        for (i = 0; i < selected.length; i++){
            if (j > 0){
                filter_string += '|';
            }
            filter_string += selected[i];
            j++;
        }
        filter_strings.push(filter_string);
    }

    // collection
    <?php if (($archive===true)&&($archive_key=='archive_collection')){ ?>
        filter_strings.push('archive=true');
        filter_strings.push('archive_key=archive_collection');
        filter_strings.push('collection=<?=rawurlencode($archive_name)?>');
    <?php } else { ?>
    if ($("#collections").val()!=""){
        filter_strings.push('collection='+encodeURIComponent($("#collections").val()));
    }
    <?php } ?>

    if ($("#key").is(":checked")){
        filter_strings.push('key=1');
    }
    if ($("#doc_date_desc").is(":checked")){
        filter_strings.push('sort_order=doc_date_desc');
    } else if ($("#doc_date_asc").is(":checked")){
        filter_strings.push('sort_order=doc_date_asc');
    } else if ($("#posted_date_desc").is(":checked")){
        filter_strings.push('sort_order=posted_date_desc');
    } else if ($("#posted_date_asc").is(":checked")){
        filter_strings.push('sort_order=posted_date_asc');
    }
    var filter_string = '';
    var j = 0;
    for (i = 0; i < filter_strings.length; i++){
        if (j > 0){
            filter_string += '&';
        }
        filter_string += filter_strings[i];
        j++;
    }
    return filter_string;
} 
var clearAllFilters = function(){
    $("#pg").val(1);
    $("#keywords").val('');
    $("#boolean_method").val('and');
    $("#doc_date_min").val('');
    $("#doc_date_max").val('');
    startDocDate.clearSelection();
    startPostedDate.clearSelection();
    endDocDate.clearSelection();
    endPostedDate.clearSelection();
    $("#posted_date_min").val('');
    $("#posted_date_max").val('');
    //$("#source-filters input:checked").prop('checked', false);
    $("#source").val('');
    //$("#source_values").val('');
    $("#document_type-filters input:checked").prop('checked', false);
    $("#category-filters input:checked").prop('checked', false);
    //$("#collection-filters input:checked").prop('checked', false);
    //$("#collections").val('');
    $("#document_type").val('');
    $("#document_type_values").val('');
    $("#category").val('');
    $("#category_values").val('');
    $("#collections").val('');
    //$("#collection_values").val('');
    $("#key").prop('checked', false);
    $("#doc_date_desc").prop('checked', true);
    $("#doc_date_asc").prop('checked', false);
    $("#posted_date_desc").prop('checked', false);
    $("#posted_date_asc").prop('checked', false);
    $("#active-filters").empty();
    $("#topics").val('');
    $(".doc-search form").submit();
}
jQuery(document).ready(function($) {
    $("#clear-all-filters").on('click', function(){
        clearAllFilters();
    });
    <?php if (($archive===true)&&($archive_key=='archive_category')){ ?>
        console.log('category archive');
        filter_string = build_filter_string();
        window.localStorage.setItem('archive_filter_string', filter_string);
    <?php } else if (($archive===true)&&($archive_key=='archive_source')){ ?>
        console.log('source archive');
        filter_string = build_filter_string();
        window.localStorage.setItem('archive_filter_string', filter_string);
    <?php } else if (($archive===true)&&($archive_key=='archive_topic')){ ?>
        console.log('topic archive');
        filter_string = build_filter_string();
        window.localStorage.setItem('archive_filter_string', filter_string);
    <?php } else if (($archive===true)&&($archive_key=='archive_collection')){ ?>
        console.log('collection archive');
        filter_string = build_filter_string();
        //alert(filter_string);
        window.localStorage.setItem('archive_filter_string', filter_string);
    <?php } else { ?>
    console.log('page load / form submitted'); // does not run when ajax is fired
        <?php 
        // you must pass a external_form hidden field from any form outside of this page for
        // any values to update the form on load
        if (isset($_POST['external_form'])){ ?>
            filter_string = build_filter_string();
            window.localStorage.setItem('init_filter_string', filter_string); 
            window.localStorage.setItem('last_filter_string', filter_string); 
            console.log("INIT LOAD DATA: " + window.localStorage.getItem('last_filter_string'));
        <?php } else { ?>
            if (window.localStorage.getItem('override_init_filter_string')){
                console.log("INIT STATE (OVERRIDE): " + window.localStorage.getItem('last_filter_string'));
                var filter_string = window.localStorage.getItem('last_filter_string');
                repop(filter_string);
            } else {
                filter_string = build_filter_string();
                window.localStorage.setItem('init_filter_string', filter_string); 
                window.localStorage.setItem('last_filter_string', filter_string); 
                console.log("INIT LOAD DATA: " + window.localStorage.getItem('last_filter_string'));
            }
        <?php } ?>
    <?php } ?>
    // load results
    $("#get-posts-overlay").fadeIn(100);
    var t = setTimeout(function(){
        var pg = $("#pg").val();
        ajaxurl = '/wp-admin/admin-ajax.php';
        url = window.location.href;
        console.log(filter_string);
        $.ajax({
            url: ajaxurl,
            cache:false,
            data: {
                action: 'get_posts',
                filters: filter_string,
                limit:20,
                pg: pg
            },
            type: 'POST'
        })
        .done(function(data) {
            //$.scrollTo(0, 100); 03012023 ferko removed per Steve - didn't want back to top when hitting back button
            $("#data-container").html(data);
            var t = setTimeout(function(){
                $("#get-posts-overlay").fadeOut(100);
            }, 250);
        });
    }, 500);

    $("body").prepend('<div class="doc-search-underlay"></div>');
    // function to build the dimensions and positioning of the modal overlays
    function sizeFilterModal(){
        var o = $(".doc-search").position();
        var w = $(window).width();
        var h = $(window).height();
        if (w > 1099){
            var s = $(window).scrollTop();
            var o = $(".doc-search").position();
            var w = $(window).width();
            var h = $(window).height();
            var t = (o.top - 38 - s);
            if (t < 0){ t = 0; }
            if (h > $(window).height()){
                h = $(window).height();
            }
            $(".filter-modal").css({
                'top':'76px',
                'left':'0px',
                'width':(w - (38*4)) + 'px',
                'padding-left': '38px',
                'height':(h - (38*5))+'px'
            });
        } else {
            $(".filter-modal").css({
                'top':'38px',
                'left':'0px',
                'width':(w - (38*3)) + 'px',
                'padding-left': '38px',
                'height':(h - (38*3))+'px'
            });
        }
    }

    function hasChildren(id){
        var doIHaveKids = false;
        if ($(id).parent().parent().parent().find('> ul').length > 0){
            doIHaveKids = true;
        }
        return doIHaveKids;
    }

    function unEntity(str){
       return $("<textarea></textarea>").html(str).text();
    }

    // reset dimensions during window resize
    $(window).on('resize', function(){
        sizeFilterModal();
    });

    // $("#search-submit").on('click', function(){
    //     $("#reset").val('true');
    //     pg = 1;
    //     url = window.location.href;
    //     var url = url.split("?")[0] + "?pg=" + pg;
    //     window.history.pushState(null, '', url);
    //     $("#reset").val('false');
    //     $(".doc-search form").submit();
    //     $.scrollTo(0, 100);
    // });

    // this closes the modal when you click on the underlay
    $("body").on('click', ".doc-search-underlay", function(){
        $(".doc-search-underlay").fadeOut(100);
        $(".filter-modal-open").fadeOut(100);
        $("body").removeClass('prevent-scrolling');
    });
    // fire the modal, show the underlay, and sets the body overflow to hidden to prevent scrolling
    $("body").on('click', "input.has-modal", function(){
        sizeFilterModal();
        $(this).parent().next().addClass('filter-modal-open');
        $(".doc-search-underlay").fadeIn(100);
        $("body").addClass('prevent-scrolling');
        $(this).parent().next().fadeIn(100);
        // if ($(this).attr('id')=='source'){
        //     var display = [];
        //     var data = [];
        //     $('#source-filters input:checked').each(function() {
        //         data.push($(this).attr('name'));
        //         display.push($(this).parent().next().html());
        //     });
        //     var value_string = '';
        //     var data_string = '';
        //     var j = 0;
        //     for (i = 0; i < display.length; i++){
        //         if (j > 0){
        //             value_string += ', ';
        //             data_string += '|';
        //         }
        //         value_string += display[i];
        //         data_string += data[i];
        //         j++;
        //     }
        //     $("#init-source-filters").val(data_string);
        // }
        if ($(this).attr('id')=='document_type'){
            var display = [];
            var data = [];
            $('#document_type-filters input:checked').each(function() {
                data.push($(this).attr('name'));
                display.push($(this).parent().next().html());
            });
            var value_string = '';
            var data_string = '';
            var j = 0;
            for (i = 0; i < display.length; i++){
                if (j > 0){
                    value_string += ', ';
                    data_string += '|';
                }
                value_string += display[i];
                data_string += data[i];
                j++;
            }
            $("#init-document_type-filters").val(data_string);
        }
        else if ($(this).attr('id')=='category'){
            var display = [];
            var data = [];
            $('#category-filters input:checked').each(function() {
                data.push($(this).attr('name'));
                let dc = $(this).parent().next().html();
                display.push(dc);

            });
            var value_string = '';
            var data_string = '';
            var j = 0;
            for (i = 0; i < display.length; i++){
                if (j > 0){
                    value_string += ', ';
                    data_string += '|';
                }
                value_string += unEntity(display[i]);
                data_string += data[i];
                j++;
            }
            $("#init-category-filters").val(data_string);
        }
        // else if ($(this).attr('id')=='collection'){
        //     var display = [];
        //     var data = [];
        //     $('#collection-filters input:checked').each(function() {
        //         data.push($(this).attr('name'));
        //         display.push($(this).parent().next().html());
        //     });
        //     var value_string = '';
        //     var data_string = '';
        //     var j = 0;
        //     for (i = 0; i < display.length; i++){
        //         if (j > 0){
        //             value_string += ', ';
        //             data_string += '|';
        //         }
        //         value_string += display[i];
        //         data_string += data[i];
        //         j++;
        //     }
        //     $("#init-collection-filters").val(data_string);
        // }
    });
    // these are the buttons inside the modals
    $("body").on('click', '.update-filters', function(){
        $(this).parent().parent().parent().fadeOut(100);
        $("body").removeClass('prevent-scrolling');
        $(".doc-search-underlay").fadeOut(100);
    });
    // the following onclick events set the display values on the sidebar fields
    // $("body").on('click', '#update-source-filter', function(){
    //     var display = [];
    //     var data = [];
    //     $('#source-filters input:checked').each(function() {
    //         data.push($(this).attr('name'));
    //         display.push($(this).parent().next().text());
    //     });
    //     var value_string = '';
    //     var data_string = '';
    //     var j = 0;
    //     for (i = 0; i < display.length; i++){
    //         if (j > 0){
    //             value_string += ', ';
    //             data_string += '|';
    //         }
    //         value_string += display[i];
    //         data_string += data[i];
    //         j++;
    //     }
    //     $("#source").val(value_string);
    //     $("#source_values").val(data_string);
    // });
    // <?php if ((isset($args['source']))&&($args['source']!="")){?>$('#update-source-filter').click();<?php }?>
    // $("#cancel-source-filter").on('click', function(){
    //     $(".doc-search-underlay").click();
    //     var t = setTimeout(function(){
    //         var initSourceValues = $("#init-source-filters").val();
    //         initSourceValues = initSourceValues.split('|');
    //         $("#source-filters input:checked").prop('checked', false);
    //         for (var i = 0; i < initSourceValues.length; i++){
    //             $("#source-filter-"+initSourceValues[i]).prop('checked', true);
    //         }
    //     }, 100);
    // });
    // $("#select-all-source-filter").on('click', function(){
    //     $("#source-filters input").prop('checked', true);
    // });
    // $("#clear-source-filter").on('click', function(){
    //     $("#source-filters input:checked").prop('checked', false);
    // });
    $("body").on('click', '#update-document_type-filter', function(){
        var display = [];
        var data = [];
        $('#document_type-filters input:checked').each(function() {
            display.push($(this).parent().next().text());
            data.push($(this).attr('name'));
        });
        var value_string = '';
        var data_string = '';
        var j = 0;
        for (i = 0; i < display.length; i++){
            if (j > 0){
                value_string += ', ';
                data_string += '|';
            }
            value_string += display[i];
            data_string += data[i];
            j++;
        }
        $("#document_type").val(value_string);
        $("#document_type_values").val(data_string);
    });
    <?php if ((isset($args['document_type']))&&($args['document_type']!="")){?>$('#update-document_type-filter').click();<?php }?>
    $("#cancel-document_type-filter").on('click', function(){
        $(".doc-search-underlay").click();
        var t = setTimeout(function(){
            var initDocumentTypeValues = $("#init-document_type-filters").val();
            initDocumentTypeValues = initDocumentTypeValues.split('|');
            $("#document_type-filters input:checked").prop('checked', false);
            for (var i = 0; i < initDocumentTypeValues.length; i++){
                $("#document_type-filter-"+initDocumentTypeValues[i]).prop('checked', true);
            }
        }, 100);
    });
    $("#select-all-document_type-filter").on('click', function(){
        $("#document_type-filters input").prop('checked', true);
    });
    $("#clear-document_type-filter").on('click', function(){
        $("#document_type-filters input:checked").prop('checked', false);
    });
    $("body").on('click', '#update-category-filter', function(){
        var display = [];
        var data = [];
        $('#category-filters input:checked').each(function() {
            display.push($(this).parent().next().text());
            data.push($(this).attr('name'));
        });
        var value_string = '';
        var data_string = '';
        var j = 0;
        for (i = 0; i < display.length; i++){
            if (j > 0){
                value_string += ', ';
                data_string += '|';
            }
            value_string += display[i];
            data_string += data[i];
            j++;
        }
        $("#category").val(value_string);
        $("#category_values").val(data_string);
    });
    <?php if ((isset($args['category']))&&($args['category']!="")){?>$('#update-category-filter').click();<?php }?>
    $("#cancel-category-filter").on('click', function(){
        $(".doc-search-underlay").click();
        var t = setTimeout(function(){
            var initCategoryValues = $("#init-category-filters").val();
            initCategoryValues = initCategoryValues.split('|');
            $("#category-filters input:checked").prop('checked', false);
            for (var i = 0; i < initCategoryValues.length; i++){
                $("#category-filter-"+initCategoryValues[i]).prop('checked', true);
            }
        }, 100);
    });   
    $("#select-all-category-filter").on('click', function(){
        $("#category-filters input").prop('checked', true);
    });
    $("#clear-category-filter").on('click', function(){
        $("#category-filters input:checked").prop('checked', false);
    });
    // $("body").on('click', '#update-collection-filter', function(){
    //     var display = [];
    //     var data = [];
    //     $('#collection-filters input:checked').each(function() {
    //         display.push($(this).parent().next().text());
    //         data.push($(this).attr('name'));
    //     });
    //     var value_string = '';
    //     var data_string = '';
    //     var j = 0;
    //     for (i = 0; i < display.length; i++){
    //         if (j > 0){
    //             value_string += ', ';
    //             data_string += '|';
    //         }
    //         value_string += display[i];
    //         data_string += data[i];
    //         j++;
    //     }
    //     $("#collection").val(value_string);
    //     $("#collection_values").val(data_string);
    // });
    // <?php if ((isset($args['collection']))&&($args['collection']!="")){?>$('#update-collection-filter').click();<?php }?>
    // $("#cancel-collection-filter").on('click', function(){
    //     $(".doc-search-underlay").click();
    //     var t = setTimeout(function(){
    //         var initCollectionValues = $("#init-collection-filters").val();
    //         initCollectionValues = initCollectionValues.split('|');
    //         $("#collection-filters input:checked").prop('checked', false);
    //         for (var i = 0; i < initCollectionValues.length; i++){
    //             $("#collection-filter-"+initCollectionValues[i]).prop('checked', true);
    //         }
    //     }, 100);
    // });  
    // $("#clear-collection-filter").on('click', function(){
    //     $("#collection-filters input:checked").prop('checked', false);
    // });
    <?php 
    // ---------------------------------------------------------
    // this is the actual form submit function that builds the filter tags, the query string, and grabs the results from algolia
    // PLEASE NOTE that this function instantiates a new history state that allows us to go back and forth. 
    // DO NOT CALL THIS FUNCTION IN THE POPSTATE LISTENER AS IT WILL CREATE A MASSIVE BLACK HOLE THAT WILL CAUSE THE UNIVERSE TO IMPLODE
    // Other than that, this can be reused pretty much anywhere we need to call new data by running the 'submit' trigger on this element
    // ---------------------------------------------------------
    ?>
    
    $("#data-container").on('click', "#search-filters-submit", function(){
        $(".doc-search form").submit();
    });

    $(".doc-search form").on('submit', function(e){
        $("#get-posts-overlay").fadeIn(100);
        e.preventDefault();
        var t = setTimeout(function(){
            var filter_string = build_filter_string();
            var pg = $("#pg").val();
            ajaxurl = '/wp-admin/admin-ajax.php';
            url = window.location.href;
            window.history.pushState(filter_string, '', url);
            window.localStorage.setItem('last_filter_string', filter_string); // reset this
            console.log("NEW HISTORY STATE: " + window.localStorage.getItem('last_filter_string'));
            // window.localStorage.setItem('current_filter_string', filter_string);
            // console.log("(SUBMIT) LAST FILTER STRING: " + window.localStorage.getItem('last_filter_string'));
            // console.log("(SUBMIT) CURRENT FILTER STRING: " + window.localStorage.getItem('current_filter_string'));
            $.ajax({
                url: ajaxurl,
                cache:false,
                data: {
                    action: 'get_posts',
                    filters: filter_string,
                    limit:20,
                    pg: pg
                },
                type: 'POST'
            })
            .done(function(data) {
                $.scrollTo(0, 100);
                $("#data-container").html(data);
                var t = setTimeout(function(){
                    $("#get-posts-overlay").fadeOut(100);
                }, 250);
            });
        }, 500);

    });
    // ---------------------------------------------------------
    // this is the click event that happens when you click on a filter tag
    // it will remove the tag, remove that key from the query string, update
    // the values shown and uncheck the input that it is associated with in the sidebar
    $("#data-container").on('click', "#active-filters a:not('.inactive, .edit')", function(){
        var data_filter = '#'+$(this).attr('data-filter');
        var filter_type = $(this).attr('data-filter-type');
        if (
            (filter_type=='document_type')
            || (filter_type=='category')
        ){
            $(this).fadeOut(100);
            $(data_filter).prop('checked', false);
            $("#update-"+filter_type+"-filter").click();
        }
        else if (filter_type=='keywords'){
            $(this).fadeOut(100);
            $("#keywords").val('');
        }
        else if (filter_type=='collection'){
            $(this).fadeOut(100);
            $("#topics").val('');
        }
        else if (filter_type=='source'){
            $(this).fadeOut(100);
            $("#topics").val('');
        }
        else if (filter_type=='topics'){
            $(this).fadeOut(100);
            $("#topics").val('');
        }
        else if (filter_type=='doc_date_min'){
            $('#active-filters a[data-filter-type="doc_date_min"]').fadeOut(100);
            $('#active-filters a[data-filter-type="doc_date_max"]').fadeOut(100);
            startDocDate.clearSelection();
            $("#doc_date_min").val('');
            $("#doc_date_max").val('');
        }    
        else if (filter_type=='doc_date_max'){
            $('#active-filters a[data-filter-type="doc_date_min"]').fadeOut(100);
            $('#active-filters a[data-filter-type="doc_date_max"]').fadeOut(100);
            endDocDate.clearSelection();
            $("#doc_date_min").val('');
            $("#doc_date_max").val('');
        } 
        else if (filter_type=='posted_date_min'){
            $('#active-filters a[data-filter-type="posted_date_min"]').fadeOut(100);
            $('#active-filters a[data-filter-type="posted_date_max"]').fadeOut(100);
            startPostedDate.clearSelection();
            $("#posted_date_min").val('');
            $("#posted_date_max").val('');
        }    
        else if (filter_type=='posted_date_max'){
            $('#active-filters a[data-filter-type="posted_date_min"]').fadeOut(100);
            $('#active-filters a[data-filter-type="posted_date_max"]').fadeOut(100);
            endDocDate.clearSelection();
            $("#posted_date_min").val('');
            $("#posted_date_max").val('');
        } 
        else if (filter_type=='key'){
            $(this).fadeOut(100);
            $(data_filter).prop('checked', false);
        }        
        $("#pg").val(1);
        $(".doc-search form").submit();
    });


    $("#data-container").on('click', "#active-filters a.edit", function(){
        var data_filter = '#'+$(this).attr('data-filter');
        var filter_type = $(this).attr('data-filter-type');
        if (
            (filter_type=='document_type')
            || (filter_type=='category')
        ){
            $("#"+filter_type).click();
        }  
        $("#pg").val(1);
        //$(".doc-search form").submit();
    });


    $(".filter-modal-fields-wrapper input[type='checkbox']").on('click', function(){
        var id = '#'+$(this).attr('id');
        var c = hasChildren(id);
        var state = $(this).is(':checked');
        if (c){
            if (state===true){
                $(id).parent().parent().parent().find('input:checkbox').prop('checked',true);
            } else {
                $(id).parent().parent().parent().find('input:checkbox').prop('checked',false);
            }
        }
    });

    $(".current-counts a.filter-by-date").on('click', function(e){
        e.preventDefault();
        clearAllFilters();
        var d = $(this).attr('data-attr-min-date');
        startDocDate.setDateRange(d, d);
        $("#doc_date_min").val(d);
        $("#doc_date_max").val(d);
        $(".doc-search form").submit();
    });

    $(".current-counts a.show-all").on('click', function(e){
        e.preventDefault();
        clearAllFilters();
        $(".doc-search form").submit();
    });

    $("#data-container").on('click', "table.documents thead th.sortable", function(){
        $("#pg").val(1);
        $("#reset").val('true');
        var order = $(this).attr('data-sort-order');
        var active = $(this).attr('data-sort-active');
        var key = $(this).attr('data-sort-key');
        if (key == 'posted_date'){
            if (order == 'desc'){
                if (active == 'true'){
                    // reverse the order
                    order = 'asc';
                    index = 'dev_pns_sortby_posted_date_asc';
                    $("#posted_date_asc").prop('checked',true);
                } else {
                    order = 'desc';
                    index = 'dev_pns_sortby_posted_date_desc';
                    $("#posted_date_desc").prop('checked',true);
                }
            } else {
                if (active == 'true'){
                    // reverse the order
                    order = 'desc';
                    index = 'dev_pns_sortby_posted_date_desc';
                    $("#posted_date_desc").prop('checked',true);
                } else {
                    order = 'asc';
                    index = 'dev_pns_sortby_posted_date_asc';
                    $("#posted_date_asc").prop('checked',true);
                }
            }
        } else if (key == 'document_date'){
            if (order == 'desc'){
                if (active == 'true'){
                    // reverse the order
                    order = 'asc';
                    index = 'dev_pns_sortby_document_date_asc';
                    $("#doc_date_asc").prop('checked',true);
                } else {
                    order = 'desc';
                    index = 'dev_pns';
                    $("#doc_date_desc").prop('checked',true);
                }
            } else {
                if (active == 'true'){
                    // reverse the order
                    order = 'desc';
                    index = 'dev_pns';
                    $("#doc_date_desc").prop('checked',true);
                } else {
                    order = 'asc';
                    index = 'dev_pns_sortby_document_date_asc';
                    $("#doc_date_asc").prop('checked',true);
                }
            }
        }
        $(".doc-search form").submit();
    });

    $("#data-container").on('click', ".pagination a", function(e){
        e.preventDefault();
        pg = $(this).attr('data-attr-load-page');
        $("#pg").val(pg);
        $(".doc-search form").submit();
        $.scrollTo(0, 100);
    });

    $("#data-container").on('click', ".gotopage button", function(e){
        e.preventDefault();
        var pg = 1;
        if ($(this).prev().val()){
            pg = parseInt($(this).prev().val());
            if (!Number.isNaN(pg)) {
                $("#pg").val(pg);
                $(".doc-search form").submit();
                $.scrollTo(0, 100);
            }
        }
        return false;
    });

    <?php if (!isset($_GET['p'])){ ?>

    <?php } ?>
});

function repop(filter_string){
    $("#pg").val(1);
    $("#keywords").val('');
    $("#boolean_method").val('and');
    $("#doc_date_min").val('');
    $("#doc_date_max").val('');
    startDocDate.clearSelection();
    startPostedDate.clearSelection();
    endDocDate.clearSelection();
    endPostedDate.clearSelection();
    $("#posted_date_min").val('');
    $("#posted_date_max").val('');
    //$("#source-filters input:checked").prop('checked', false);
    $("#source").val('');
    //$("#source_values").val('');
    $("#document_type-filters input:checked").prop('checked', false);
    $("#category-filters input:checked").prop('checked', false);
    //$("#collection-filters input:checked").prop('checked', false);
    $("#document_type").val('');
    $("#document_type_values").val('');
    $("#category").val('');
    $("#category_values").val('');
    $("#collections").val('');
    //$("#collection_values").val('');
    $("#key").prop('checked', false);
    $("#doc_date_desc").prop('checked', true);
    $("#doc_date_asc").prop('checked', false);
    $("#posted_date_desc").prop('checked', false);
    $("#posted_date_asc").prop('checked', false);
    $("#active-filters").empty();
    $("#topics").val('');
    var state = filter_string.split('&'); 
    for (i = 0; i < state.length; i++){
        kv = state[i].split('=');
        k = kv[0];
        v = kv[1];
        if (k == 'pg'){
            $("#pg").val(v);
        }
        if (k == 'keywords'){
            $("#keywords").val(decodeURIComponent(v));
        }
        if (k == 'boolean_method'){
            $("#boolean_method").val(v);
        }
        if (k == 'doc_date_min'){
            $("#doc_date_min").val(v);
        }
        if (k == 'doc_date_max'){
            $("#doc_date_max").val(v);
        }
        if (k == 'posted_date_min'){
            $("#posted_date_min").val(v);
        }
        if (k == 'posted_date_max'){
            $("#posted_date_max").val(v);
        }
        if (k == 'topics'){
            $("#topics").val(decodeURIComponent(v));
        }
        var ddmn = $("#doc_date_min").val();
        if (ddmn != 0){
            var ddmx = $("#doc_date_max").val();
            startDocDate.setDateRange($("#doc_date_min").val(), $("#doc_date_max").val());
        }
        var pdmn = $("#posted_date_min").val();
        if (pdmn != 0){
            var pdmx = $("#posted_date_max").val();
            startPostedDate.setDateRange($("#posted_date_min").val(), $("#posted_date_max").val());
        }
        if (k == 'source'){
            $("#source").val(decodeURIComponent(v));
        }
        if (k == 'document_type'){
            document_types = v.split('|');
            var document_typeNames = [];
            for (s = 0; s < document_types.length; s++){
                $("#document_type-filters input[name='"+document_types[s]+"'").prop('checked', true);
                document_typeNames.push($("#document_type-filters input[name='"+document_types[s]+"'").parent().next().html());
            }
            document_typeNames = document_typeNames.join(', ');
            $("#document_type").val(document_typeNames);
            $("#document_type_values").val(v);
        }
        if (k == 'category'){
            categories = v.split('|');
            var categoryNames = [];
            for (s = 0; s < categories.length; s++){
                $("#category-filters input[name='"+categories[s]+"'").prop('checked', true);
                categoryNames.push($("#category-filters input[name='"+categories[s]+"'").parent().next().html());
            }
            categoryNames = categoryNames.join(', ');
            $("#category").val(categoryNames);
            $("#category_values").val(v);
        }
        if (k == 'collection'){
            $("#collections").val(decodeURIComponent(v));
        }
        if (k == 'key'){
            if (v == 1){
                $("#key").prop('checked', true);
            } else {
                $("#key").prop('checked', false);
            }
        }
        if (k == 'sort_order'){
            $("#doc_date_desc").prop('checked', false);
            $("#doc_date_asc").prop('checked', false);
            $("#posted_date_desc").prop('checked', false);
            $("#posted_date_asc").prop('checked', false);
            if (v == 'doc_date_desc'){
                $("#doc_date_desc").prop('checked', true);
            } else if (v == 'doc_date_asc'){
                $("#doc_date_asc").prop('checked', true);
            } else if (v == 'posted_date_desc'){
                $("#posted_date_desc").prop('checked', true);
            } else if (v == 'posted_date_asc'){
                $("#posted_date_asc").prop('checked', true);
            }
        }
    }
}

window.addEventListener('popstate', function(e) {
    if (window.history.state!==null){
        console.log("LAST STATE (HISTORY): " + window.history.state);
        var filter_string = e.state
    } else {
        if (window.localStorage.getItem('override_init_filter_string')){
            console.log("INIT STATE (OVERRIDE): " + window.localStorage.getItem('init_filter_string'));
            var filter_string = window.localStorage.getItem('override_init_filter_string');
            window.localStorage.setItem('init_filter_string', filter_string)
            window.localStorage.clear('override_init_filter_string');
        } else {
            console.log("INIT STATE (STORAGE): " + window.localStorage.getItem('init_filter_string'));
            var filter_string = window.localStorage.getItem('init_filter_string');
        }
        <?php if ((isset($archive))&&($archive===true)){ ?>
            console.log("INIT STATE (ARCHIVE): " + window.localStorage.getItem('archive_filter_string'));
            var filter_string = window.localStorage.getItem('archive_filter_string');
        <?php } ?>
    }

    $("#pg").val(1);
    $("#keywords").val('');
    $("#boolean_method").val('and');
    $("#doc_date_min").val('');
    $("#doc_date_max").val('');
    startDocDate.clearSelection();
    startPostedDate.clearSelection();
    endDocDate.clearSelection();
    endPostedDate.clearSelection();
    $("#posted_date_min").val('');
    $("#posted_date_max").val('');
    //$("#source-filters input:checked").prop('checked', false);
    $("#source").val('');
    //$("#source_values").val('');
    $("#document_type-filters input:checked").prop('checked', false);
    $("#category-filters input:checked").prop('checked', false);
    //$("#collection-filters input:checked").prop('checked', false);
    $("#document_type").val('');
    $("#document_type_values").val('');
    $("#category").val('');
    $("#category_values").val('');
    $("#collections").val('');
    //$("#collection_values").val('');
    $("#key").prop('checked', false);
    $("#doc_date_desc").prop('checked', true);
    $("#doc_date_asc").prop('checked', false);
    $("#posted_date_desc").prop('checked', false);
    $("#posted_date_asc").prop('checked', false);
    $("#active-filters").empty();
    $("#topics").val('');
    var state = filter_string.split('&'); 

    for (i = 0; i < state.length; i++){
        kv = state[i].split('=');
        k = kv[0];
        v = kv[1];
        if (k == 'pg'){
            $("#pg").val(v);
        }
        if (k == 'keywords'){
            $("#keywords").val(decodeURIComponent(v));
        }
        if (k == 'boolean_method'){
            $("#boolean_method").val(v);
        }
        if (k == 'doc_date_min'){
            $("#doc_date_min").val(v);
        }
        if (k == 'doc_date_max'){
            $("#doc_date_max").val(v);
        }
        if (k == 'posted_date_min'){
            $("#posted_date_min").val(v);
        }
        if (k == 'posted_date_max'){
            $("#posted_date_max").val(v);
        }
        if (k == 'topics'){
            $("#topics").val(v);
        }
        var ddmn = $("#doc_date_min").val();
        if (ddmn != 0){
            var ddmx = $("#doc_date_max").val();
            startDocDate.setDateRange($("#doc_date_min").val(), $("#doc_date_max").val());
        }
        var pdmn = $("#posted_date_min").val();
        if (pdmn != 0){
            var pdmx = $("#posted_date_max").val();
            startPostedDate.setDateRange($("#posted_date_min").val(), $("#posted_date_max").val());
        }
        if (k == 'source'){
            $("#source").val(v);
        }
        if (k == 'document_type'){
            document_types = v.split('|');
            var document_typeNames = [];
            for (s = 0; s < document_types.length; s++){
                $("#document_type-filters input[name='"+document_types[s]+"'").prop('checked', true);
                document_typeNames.push($("#document_type-filters input[name='"+document_types[s]+"'").parent().next().html());
            }
            document_typeNames = document_typeNames.join(', ');
            $("#document_type").val(document_typeNames);
            $("#document_type_values").val(v);
        }
        if (k == 'category'){
            categories = v.split('|');
            var categoryNames = [];
            for (s = 0; s < categories.length; s++){
                $("#category-filters input[name='"+categories[s]+"'").prop('checked', true);
                categoryNames.push($("#category-filters input[name='"+categories[s]+"'").parent().next().html());
            }
            categoryNames = categoryNames.join(', ');
            $("#category").val(categoryNames);
            $("#category_values").val(v);
        }
        if (k == 'collection'){
            $("#collections").val(v);
        }
        if (k == 'key'){
            if (v == 1){
                $("#key").prop('checked', true);
            } else {
                $("#key").prop('checked', false);
            }
        }
        if (k == 'sort_order'){
            $("#doc_date_desc").prop('checked', false);
            $("#doc_date_asc").prop('checked', false);
            $("#posted_date_desc").prop('checked', false);
            $("#posted_date_asc").prop('checked', false);
            if (v == 'doc_date_desc'){
                $("#doc_date_desc").prop('checked', true);
            } else if (v == 'doc_date_asc'){
                $("#doc_date_asc").prop('checked', true);
            } else if (v == 'posted_date_desc'){
                $("#posted_date_desc").prop('checked', true);
            } else if (v == 'posted_date_asc'){
                $("#posted_date_asc").prop('checked', true);
            }
        }
    }
    // load results
    $("#get-posts-overlay").fadeIn(100);
    var t = setTimeout(function(){
        //var filter_string = window.localStorage.getItem('current_filter_string');
        var pg = $("#pg").val();
        ajaxurl = '/wp-admin/admin-ajax.php';
        $.ajax({
            url: ajaxurl,
            cache:false,
            data: {
                action: 'get_posts',
                filters: filter_string,
                limit:10,
                pg: pg
            },
            type: 'POST'
        })
        .done(function(data) {
            $.scrollTo(0, 100);
            $("#data-container").html(data);
            var t = setTimeout(function(){
                $("#get-posts-overlay").fadeOut(100);
            }, 250);
        });
    }, 500);
});
</script>
