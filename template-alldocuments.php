<?php

global $wpdb;
get_header();
// set the values for the date keywords so we can get the counts for the header
$today = date("m/d/Y");
$yesterday = date("m/d/Y", strtotime('yesterday'));
//$last_week = date("m/d/Y", strtotime('today - 7 days')); // is this being used for anything?
$active_filters = false;
require get_template_directory().'/inc/build_params.php';
if (!isset($pg)){
    $pg=1;
}
?>
<main class="main" id="content">

    <header class="content-header all-documents">
        <div class="header-top interior">
            <h1>Documents</h1>
            <div class="current-counts">
                <?php   
                // the get_total_count function calls out to algolia - it's a special function call that only returns a count
                // see the function in helpers for more info
                $filters = array(
                    'doc_date_min' => $today,
                    'doc_date_max' => $today,
                );
                $todays_docs = get_total_count($filters);
                $filters = array(
                    'doc_date_min' => $yesterday,
                    'doc_date_max' => $yesterday,
                );
                $yesterdays_docs = get_total_count($filters);
                ?>                                
                <!-- <ul class="display-counts">
                    <li><a href="/all-source-documents/" class="filter-by-date" data-attr-min-date="<?=$today?>">Today (<?=$todays_docs?>)</a></li>
                    <li><a href="/all-source-documents/" class="filter-by-date" data-attr-min-date="<?=$yesterday?>">Yesterday (<?=$yesterdays_docs?>)</a></li>
                    <li><a href="/all-source-documents/" class="show-all"><span>All (<?=get_total_count(null)?>)</a></li>
                </ul> -->
            </div>
        </div>
    </header>

    <div class="interior-content-wrapper search-results">
        
        <section class="content">
            <div class="page-content" id="data-container"></div>
            <div id="get-posts-overlay"></div>
        </section> <!-- /.content -->

        <?php 
        // pass which fields you want to appear in the sidebar search
        // (this allows you to have different fields on different pages)
        $fields = array(
            'include_keywords' => true,
            'include_date_range' => true,
            'include_source' => true,
            'include_document_type' => true,
            'include_collection' => true,
            'include_category' => true,
            'include_topic' => true,
            'include_sort' => true
        );
        // pass an array into the sidebar so we know we're dealing with an archive for the ajax call
        $archive_params = array(
            'archive' => false
        );
        get_sidebar('doc_search', array_merge($fields, $archive_params, $params)); 
        ?>
    </div>
</main>
<?php
get_footer();
?>