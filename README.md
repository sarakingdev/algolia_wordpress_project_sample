## Algolia / Wordpress Project

## Description
This is a project that was started over a year ago - it is a website that collects and categorizes thousands of publically available news articles. Being able to search these articles quickly was a primary requirement for this client so we decided on Algolia to offload as much as we could from Wordpress. A custom plugin was created to index articles into Algolia as they were published in Wordpress. 

## Project status
The site is still in development and going through user testing. 
