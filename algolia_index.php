<?php



// algolia index

$debug = true;

// building the index for algolia
if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php')) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');
}
require $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

use ICanBoogie\Inflector;
$inflector = Inflector::get(Inflector::DEFAULT_LOCALE);

global $wpdb;
global $algolia;

$client = Algolia\AlgoliaSearch\SearchClient::create(
  'xxxxxx',
  'xxxxxx'
);

$index = $client->initIndex('dev_pns');

function clean_content($string) { 
    if ($string!=''){
        $string = preg_replace ('/<[^>]*>/', ' ', $string); 
        $string = str_replace("\r", '', $string);
        $string = str_replace("\n", ' ', $string);
        $string = str_replace("\t", ' ', $string);
        $string = trim(preg_replace('/ {2,}/', ' ', $string));
        $string = strip_shortcodes($string);
    } else {
        $string = '';
    }

    return $string; 
}



$post_index = array();

// ------------------------------------------------------------------------------------------------------------------------
// SOURCE DOCUMENTS

//$posts = "select * from wp_posts where post_type = 'sourcedocument' and post_status = 'publish' and ID = '1359' limit 1 ";

if ((isset($_GET['post_type']))&&($_GET['post_type']=='sourcedocuments')){

    $limit = 50;
    $offset = 0;
    $i = 1;
    $j = 0;

    if (isset($_GET['offset'])){
        $offset = $_GET['offset'];
    }

    //$sourcedocuments = "select * from wp_posts where post_type = 'sourcedocument' and post_status = 'publish' and ID = '40358' limit $offset,$limit";

    $total_entries = "select * from wp_posts where post_type = 'sourcedocument' and post_status = 'publish'";
    $total_entries = $wpdb->get_results($total_entries);
    $total_entries = count($total_entries);

    $sourcedocuments = "select * from wp_posts where post_type = 'sourcedocument' and post_status = 'publish' limit $offset,$limit";
    $sourcedocuments = $wpdb->get_results($sourcedocuments);

    $post_keywords = array();

    foreach ($sourcedocuments as $post){

        // document areas of investigation
        $post_tags = array();
        $tags = "select 
                wp_terms.name,
                wp_terms.slug,
                wp_terms.term_id,
                wp_term_taxonomy.parent
            from
                wp_terms, wp_term_taxonomy, wp_term_relationships 
            where
                wp_term_relationships.object_id = '$post->ID' 
                and wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id 
                and wp_term_taxonomy.taxonomy = 'post_tag' 
                and wp_term_taxonomy.term_id = wp_terms.term_id";
        $tags = $wpdb->get_results($tags);

        foreach ($tags as $key => $tag){
            $post_tags[] = array(
                'name' => $tag->name,
                'term_id' => $tag->term_id,
                'slug' => $tag->slug,
                'parent' => $tag->parent
            );
        }

        $post_categories = array();
        $categories = "select 
                wp_terms.name,
                wp_terms.slug,
                wp_terms.term_id,
                wp_term_taxonomy.parent
            from
                wp_terms, wp_term_taxonomy, wp_term_relationships 
            where
                wp_term_relationships.object_id = '$post->ID' 
                and wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id 
                and wp_term_taxonomy.taxonomy = 'newscategory' 
                and wp_term_taxonomy.term_id = wp_terms.term_id";
        $categories = $wpdb->get_results($categories);

        foreach ($categories as $key => $category){
            $post_categories[] = array(
                'name' => $category->name,
                'term_id' => $category->term_id,
                'slug' => $category->slug,
                'parent' => $category->parent
            );
        }

        $collection_keywords = array();

        $post_collections = array();
        $collections = "select 
                wp_terms.name,
                wp_terms.slug,
                wp_terms.term_id,
                wp_term_taxonomy.parent
            from
                wp_terms, wp_term_taxonomy, wp_term_relationships 
            where
                wp_term_relationships.object_id = '$post->ID' 
                and wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id 
                and wp_term_taxonomy.taxonomy = 'collection' 
                and wp_term_taxonomy.term_id = wp_terms.term_id";
        $collections = $wpdb->get_results($collections);
        foreach ($collections as $key => $collection){

            $keywords_from_this_collection = get_keywords($collection->name);
            foreach ($keywords_from_this_collection as $keyword){
                $collection_keywords[] = $keyword;
            }

            $post_collections[] = array(
                'name' => $collection->name,
                'term_id' => $collection->term_id,
                'slug' => $collection->slug,
                'parent' => $collection->parent
            );
        }

        foreach ($collection_keywords as $keyword){   
            $post_keywords[] = array(
                'keyword' => $keyword,
            );

            $plural_keyword = $inflector->pluralize($keyword);
            if ($plural_keyword){
                $post_keywords[] = array(
                    'keyword' => $plural_keyword
                );
            }
            $singular_keyword = $inflector->singularize($keyword);
            if ($singular_keyword){
                $post_keywords[] = array(
                    'keyword' => $singular_keyword
                );
            }
            
        }

        $post_documenttypes = array();
        $documenttypes = "select 
                wp_terms.name,
                wp_terms.slug,
                wp_terms.term_id,
                wp_term_taxonomy.parent
            from
                wp_terms, wp_term_taxonomy, wp_term_relationships 
            where
                wp_term_relationships.object_id = '$post->ID' 
                and wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id 
                and wp_term_taxonomy.taxonomy = 'documenttype' 
                and wp_term_taxonomy.term_id = wp_terms.term_id";
        $documenttypes = $wpdb->get_results($documenttypes);
        foreach ($documenttypes as $key => $documenttype){
            $post_documenttypes[] =  array(
                'name' => $documenttype->name,
                'term_id' => $documenttype->term_id,
                'slug' => $documenttype->slug,
                'parent' => $documenttype->parent
            );
        }

        $post_primarysources = array();

        $source_keywords = array();

        $primarysources = "select 
                wp_terms.name,
                wp_terms.slug,
                wp_terms.term_id,
                wp_term_taxonomy.parent
            from
                wp_terms, wp_term_taxonomy, wp_term_relationships 
            where
                wp_term_relationships.object_id = '$post->ID' 
                and wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id 
                and wp_term_taxonomy.taxonomy = 'primarysource' 
                and wp_term_taxonomy.term_id = wp_terms.term_id";
        $primarysources = $wpdb->get_results($primarysources);
        foreach ($primarysources as $key => $primarysource){

            $keywords_from_this_source = get_keywords($primarysource->name);
            foreach ($keywords_from_this_source as $keyword){
                $source_keywords[] = $keyword;
            }

            $post_primarysources[] =  array(
                'name' => $primarysource->name,
                'term_id' => $primarysource->term_id,
                'slug' => $primarysource->slug,
                'parent' => $primarysource->parent
            );
        }

        foreach ($source_keywords as $keyword){   
            $post_keywords[] = array(
                'keyword' => $keyword,
            );

            $plural_keyword = $inflector->pluralize($keyword);
            if ($plural_keyword){
                $post_keywords[] = array(
                    'keyword' => $plural_keyword
                );
            }
            $singular_keyword = $inflector->singularize($keyword);
            if ($singular_keyword){
                $post_keywords[] = array(
                    'keyword' => $singular_keyword
                );
            }
            
        }
        
        // sak - 06092022 - add in topics to index
        // document areas of investigation
        $post_topics = array();

        $topic_keywords = array();

        $topics = "select 
                wp_terms.name,
                wp_terms.slug,
                wp_terms.term_id,
                wp_term_taxonomy.parent
            from
                wp_terms, wp_term_taxonomy, wp_term_relationships 
            where
                wp_term_relationships.object_id = '$post->ID' 
                and wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id 
                and wp_term_taxonomy.taxonomy = 'topic' 
                and wp_term_taxonomy.term_id = wp_terms.term_id";
        $topics = $wpdb->get_results($topics);
        $ac_topics = array();
        foreach ($topics as $key => $topic){
            $keywords_from_this_topic = get_keywords($topic->name);
            foreach ($keywords_from_this_topic as $keyword){
                $topic_keywords[] = $keyword;
            }
            $post_topics[] = array(
                'name' => $topic->name,
                'term_id' => $topic->term_id,
                'slug' => $topic->slug,
                'parent' => $topic->parent
            );
        }

        foreach ($topic_keywords as $keyword){   
            $post_keywords[] = array(
                'keyword' => $keyword,
            );

            $plural_keyword = $inflector->pluralize($keyword);
            if ($plural_keyword){
                $post_keywords[] = array(
                    'keyword' => $plural_keyword
                );
            }
            $singular_keyword = $inflector->singularize($keyword);
            if ($singular_keyword){
                $post_keywords[] = array(
                    'keyword' => $singular_keyword
                );
            }
            
        }

        $pns_keydoc = 0;
        $keydoc = $wpdb->get_col("select * from wp_postmeta where post_id = '$post->ID' and meta_key = 'pns_keydoc' limit 1", 3);
        if ($keydoc){
            if ($keydoc[0]){
                $pns_keydoc = $keydoc[0];
            }
        }

        $pns_document_date = null;
        $pns_document_date_display = 'mdy';

        $by_monthyear = false;
        $by_yearonly = false;

        // pns_display_month - Display Month/Year Only
        $pns_display_month = $wpdb->get_col("select * from wp_postmeta where post_id = '$post->ID' and meta_key = 'pns_display_month' limit 1", 3);
        if ($pns_display_month){
            if ($pns_display_month[0]==1){
                $by_monthyear = true;
                $pns_document_date_display = 'my';
            }
        }

        // pns_display_year - Display Year Only
        $pns_display_year = $wpdb->get_col("select * from wp_postmeta where post_id = '$post->ID' and meta_key = 'pns_display_year' limit 1", 3);
        if ($pns_display_year){
            if ($pns_display_year[0]==1){
                $by_yearonly = true;
                $pns_document_date_display = 'y';
            }
        }

        if (($by_monthyear===true)&&($by_yearonly===true)){
            $by_monthyear = true;
            $by_yearonly = false;
            $pns_document_date_display = 'my';
        }

        $document_date = $wpdb->get_col("select * from wp_postmeta where post_id = '$post->ID' and meta_key = 'pns_document_date' limit 1", 3);
        if ($document_date){
            $pns_document_date = $document_date[0];
        }

        $pns_docid = null;
        $docid = $wpdb->get_col("select * from wp_postmeta where post_id = '$post->ID' and meta_key = 'pns_docid' limit 1", 3);
        if ($docid){
            $pns_docid = $docid[0];
        }

        $media = array();
        $files = $wpdb->get_results("select * from wp_postmeta where post_id = '$post->ID' and meta_key like 'pns_media_assets_%' and meta_key like '%_pns_media_assets_file'");
        $links = $wpdb->get_results("select * from wp_postmeta where post_id = '$post->ID' and meta_key like 'pns_media_assets_%' and meta_key like '%_pns_media_assets_linktext'");
        $captions = $wpdb->get_results("select * from wp_postmeta where post_id = '$post->ID' and meta_key like 'pns_media_assets_%' and meta_key like '%_pns_media_assets_caption'");
        $assets = array();

        $caption_keywords = array();

        foreach ($files as $key => $file){
            if ($file->meta_value != ''){
                // we have a file added - get it's name
                $filename = $wpdb->get_results("select meta_value from wp_postmeta where meta_key = '_wp_attached_file' and post_id = '$file->meta_value'");
                $filename = $filename[0]->meta_value;
                $assets[$key]['filename'] = $filename;
            }
            // this link 
            $this_link = $links[$key];
            $assets[$key]['linktext'] = $this_link;
            // this caption
            $this_caption = $captions[$key];
            $assets[$key]['caption'] = $this_caption;
            $keywords_from_caption = get_keywords($this_caption->meta_value);
            foreach ($keywords_from_caption as $keyword){
                $caption_keywords[] = $keyword;
            }

        }

        foreach ($caption_keywords as $keyword){  
            $plural_keyword = $inflector->pluralize($keyword);
            if ($plural_keyword){
                $post_keywords[] = array(
                    'keyword' => $plural_keyword
                );
            }
            $singular_keyword = $inflector->singularize($keyword);
            if ($singular_keyword){
                $post_keywords[] = array(
                    'keyword' => $singular_keyword
                );
            } 
            $post_keywords[] = array(
                'keyword' => $keyword,
            );
        }

        $crafted_excerpt = limit_text(clean_content($post->post_content), 75); // was 50 - 06062022 - sak - bh #121 - trying to get to 4 lines
        $year = date('Y', strtotime($pns_document_date));
        $month = date('n', strtotime($pns_document_date));

        $keywords_from_title = get_keywords($post->post_title);
        $keywords_from_content = get_keywords($post->post_content);
        $keywords = array_unique(array_merge($keywords_from_title, $keywords_from_content));

        foreach ($keywords as $keyword){
            $post_keywords[] = array(
                'keyword' => $keyword,
            );
            $plural_keyword = $inflector->pluralize($keyword);
            if ($plural_keyword){
                $post_keywords[] = array(
                    'keyword' => $plural_keyword
                );
            }
            $singular_keyword = $inflector->singularize($keyword);
            if ($singular_keyword){
                $post_keywords[] = array(
                    'keyword' => $singular_keyword
                );
            }
        }

        $post_keywords = array_unique($post_keywords, SORT_REGULAR);
        $post_keywords = array_slice($post_keywords, 0, 1000);

        $this_post = array(
            'objectID' => (int) $post->ID,
            'record_type' => 'sourcedocument',
            'document_date' => (int) date('U', strtotime($pns_document_date)),
            'document_date_display' => $pns_document_date_display,
            'posted_date' => (int) date('U', strtotime($post->post_date)),
            'year' => $year,
            'month' => $month,
            'post_id' => (int) $post->ID,
            'doc_id' => $pns_docid,
            'title' => $post->post_title,
            'slug' => $post->post_name,
            'excerpt' => $crafted_excerpt,
            'content' => $post->post_content,
            'keywords' => $post_keywords,
            'tags' => $post_tags,
            'collections' => $post_collections,
            'categories' => $post_categories,
            'source' => $post_primarysources,
            'type' => $post_documenttypes,
            'topics' => $post_topics,
            'key' => (int) $pns_keydoc,
            'media_assets' => $assets
        );
        $post_index[$post->ID] = $this_post;
    }
    
    foreach ($post_index as $objectID => $record){
        $index->saveObject($record, [
            'objectIDKey' => 'objectID'
        ]);
        // debug($record);
        if ($i == $limit){
            $newOffset = $offset + 50;
            echo '<a id="next_button" href="https://primary-news:8890/algolia_index.php?post_type='.$_GET['post_type'].'&offset='.$newOffset.'"><button>Next</button></a><br><br>';
        }
        $j++;
        $i++;
    }
    
    echo 'Entries Processed: ' . $j;

    echo '<br><br>Total Entries Processed: '.$newOffset.' of ' . $total_entries;

    echo "<Br><br>Done";

    echo "<script>window.onload = function() {";
        echo "console.log('next set');";
        echo "document.getElementById('next_button').click();";
    echo "};</script>";

}

// END SOURCEDOCUMENTS
// ------------------------------------------------------------------------------------------------------------------------

if ((isset($_GET['post_type']))&&($_GET['post_type']=='topics')){

    // https://primary-news:8890/algolia_index.php?post_type=topics&offset=0
    $offset = 0;
    $limit = 50;
    $i = 1;
    $j = 0;

    if (isset($_GET['offset'])){
        $offset = $_GET['offset'];
    }

    $index = $client->initIndex('dev_topics');
    $post_topics = array();
    $topics = "select 
            wp_terms.term_id,
            wp_terms.name
        from
            wp_terms, wp_term_taxonomy 
        where 
            wp_term_taxonomy.taxonomy = 'topic' 
            and wp_term_taxonomy.term_id = wp_terms.term_id 
        limit $offset,$limit";
    $topics = $wpdb->get_results($topics);
    $all_topics = array();
    foreach ($topics as $topic){
        $record = array('objectID' => $topic->term_id, 'topic' => $topic->name);
        //debug($record);
        $index->saveObject($record, [
            'objectIDKey' => 'objectID'
        ]);
        if ($i == $limit){
            $newOffset = $offset + 50;
            echo '<a href="https://primary-news:8890/algolia_index.php?post_type='.$_GET['post_type'].'&offset='.$newOffset.'"><button>Next</button></a>';
        }
        $j++;
        $i++;
    }

    echo '<br><br>Entries processed: ' . $j;

    echo "<Br><br>Done";

}

if ((isset($_GET['post_type']))&&($_GET['post_type']=='collections')){
    
    $offset = 0;
    $limit = 50;
    $i = 1;
    $j = 0;

    if (isset($_GET['offset'])){
        $offset = $_GET['offset'];
    }

    $index = $client->initIndex('dev_collections');
    $post_collections = array();
    $collections = "select 
            wp_terms.term_id,
            wp_terms.name
        from
            wp_terms, wp_term_taxonomy 
        where 
            wp_term_taxonomy.taxonomy = 'collection' 
            and wp_term_taxonomy.term_id = wp_terms.term_id 
        limit $offset,$limit";
    $collections = $wpdb->get_results($collections);
    $all_collections = array();
    foreach ($collections as $collection){
        $record = array('objectID' => $collection->term_id, 'collection' => $collection->name);
        //debug($record);
        $index->saveObject($record, [
            'objectIDKey' => 'objectID'
        ]);
        if ($i == $limit){
            $newOffset = $offset + 50;
            echo '<a href="https://primary-news:8890/algolia_index.php?post_type='.$_GET['post_type'].'&offset='.$newOffset.'"><button>Next</button></a><br><br>';
        }
        $j++;
        $i++;
    }

    echo 'Entries processed: ' . $j;

    echo "<Br><br>Done";

}

if ((isset($_GET['post_type']))&&($_GET['post_type']=='source')){

    $offset = 0;
    $limit = 50;
    $i = 1;
    $j = 0;

    if (isset($_GET['offset'])){
        $offset = $_GET['offset'];
    }

    $index = $client->initIndex('dev_sources');
    $post_sources = array();
    $sources = "select 
            wp_terms.term_id,
            wp_terms.name
        from
            wp_terms, wp_term_taxonomy 
        where 
            wp_term_taxonomy.taxonomy = 'primarysource' 
            and wp_term_taxonomy.term_id = wp_terms.term_id 
        limit $offset,$limit";
    $sources = $wpdb->get_results($sources);
    $all_sources = array();
    foreach ($sources as $source){
        $record = array('objectID' => $source->term_id, 'source' => $source->name);
        //debug($record);
        $index->saveObject($record, [
            'objectIDKey' => 'objectID'
        ]);
        if ($i == $limit){
            $newOffset = $offset + 50;
            echo '<a href="https://primary-news:8890/algolia_index.php?post_type='.$_GET['post_type'].'&offset='.$newOffset.'"><button>Next</button></a><br><br>';
        }
        $j++;
        $i++;
    }

    echo 'Entries processed: ' . $j;

    echo "<Br><br>Done";

}
// END TOPICS
// ------------------------------------------------------------------------------------------------------------------------

?>