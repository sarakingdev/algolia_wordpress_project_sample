<?php

// ----------------------------------------------------------------------
// this is the most important piece of this puzzle
// we're taking an object/array containing all the requested filters and turns it
// into a string that the algolia search api understands
// sak - 06102022 - we're changing the way the keyword search is performed
// :: during indexing, we're collecting "keywords" from each post and saving them as facets into algolia
// :: the new keyword "search" is now a facet filter rather than a true keyword search

function build_filter_string($filters){

    global $wpdb;

    $keywords = '';
    $topics = '';
    $collections = '';
    $search_filters = array();

    // check the value of the "featured" key - just make sure it exists
    if (!array_key_exists('key', $filters)){
        $filters['key'] = 0;
    }

    // default record_type tp sourcedocument if it doesn't exist
    // ... and it shouldn't
    if (!array_key_exists('record_type', $filters)){
        $filters['record_type'] = 'sourcedocument';
    }


    $boolean_method = 'and';
    foreach ($filters as $filter_name => $filter_value){
        if ($filter_name == 'boolean_method'){
            $boolean_method = $filter_value;
        }
    }

    foreach ($filters as $filter_name => $filter_value){
        
        // new method for handling keywords
        // sak - 06102022
        $this_filter = false;

        if (($filter_name == 'keywords')&&($boolean_method!='exact')){

            $keywords = $filter_value;
            $keywords = get_keywords($keywords, 1);

            if (count($keywords)>0){
                if (count($keywords)==1){
                    $this_filter = '(keywords.keyword:'.$keywords[0].')';
                } else {
                    $keyword_options = array();
                    foreach ($keywords as $keyword){
                        $keyword_options[] = '(keywords.keyword:'.$keyword.')';
                    }
                    if ($boolean_method == 'and'){
                        $keyword_options = implode(' AND ', $keyword_options);
                    } else if ($boolean_method == 'or'){
                        $keyword_options = implode(' OR ', $keyword_options);
                    }   
                    $this_filter = '(' . $keyword_options . ')';
                }
            }
            if ($this_filter){
                $search_filters[] = $this_filter;
            }

        } 

        // something weird is happening when passing 0 here - the counts are off - some entries are apparently not including the key?
        if ($filter_name == 'key'){
            if ($filter_value == 0){ // we're including both featured and non-featured
                //$this_filter = '(key:0 OR key:1)';
            } elseif ((int)$filter_value == 1){ // only include featured posts
                $this_filter = '(key:1)';
                $search_filters[] = $this_filter;
            } else if ((int)$filter_value == 2){ // only inlcude non-featured posts
                $this_filter = '(key:0)';
                $search_filters[] = $this_filter;
            }
        }

        else if ($filter_name == 'doc_date_min'){
            $doc_date_min = $filters['doc_date_min'];
            if ($doc_date_min==0){

            } else {
                $doc_date_min = strtotime($doc_date_min);
                $search_filters[] = '(document_date>=' . $doc_date_min . ')';
                // this allows us to pass only a single day (i.e. today or yesterday) and set the max_date automatically
                if (!array_key_exists('doc_date_max', $filters)){
                    $current_year = date('Y');
                    $doc_date_max = strtotime('12/31/'.$current_year); // to account for all of the docs that have been posted in the "future"
                    $doc_date_max = $doc_date_max + ((60*60*24) - 1); // add 23 hours, 59 minutes, and 59 seconds to include all of the max day
                    $search_filters[] = '(document_date<=' . $doc_date_max . ')';
                }
            }
        }
        else if ($filter_name == 'doc_date_max'){
            $doc_date_max = $filters['doc_date_max'];
            $doc_date_max = strtotime($doc_date_max);
            //$doc_date_max = strtotime('12/31/2999'); // to account for all of the docs that have been posted in the "future"
            $doc_date_max = $doc_date_max + ((60*60*24) - 1); // add 23 hours, 59 minutes, and 59 seconds to include all of the max day
            $search_filters[] = '(document_date<=' . $doc_date_max . ')';
        }

        else if ($filter_name == 'posted_date_min'){
            $posted_date_min = $filters['posted_date_min'];
            if ($posted_date_min==0){

            } else {
                $posted_date_min = strtotime($posted_date_min);
                $search_filters[] = '(posted_date>=' . $posted_date_min . ')';
                // this allows us to pass only a single day (i.e. today or yesterday) and set the max_date automatically
                if (!array_key_exists('posted_date_max', $filters)){
                    //$posted_date_max = $filters['posted_date_max'];
                    $current_year = date('Y');
                    $posted_date_max = strtotime('12/31/'.$current_year); // to account for all of the docs that have been posted in the "future"
                    $posted_date_max = $posted_date_max + ((60*60*24) - 1); // add 23 hours, 59 minutes, and 59 seconds to include all of the max day
                    $search_filters[] = '(posted_date<=' . $posted_date_max . ')';
                }
            }
        }

        else if ($filter_name == 'posted_date_max'){
            $posted_date_max = $filters['posted_date_max'];
            $posted_date_max = strtotime($posted_date_max);
            if ($posted_date_max==0){
                $current_year = date('Y');
                $posted_date_max = '12/31/'.$current_year;
            }
            $posted_date_max = $posted_date_max + ((60*60*24) - 1); // add 23 hours, 59 minutes, and 59 seconds to include all of the max day
            $search_filters[] = '(posted_date<=' . $posted_date_max . ')';
        }

        else if ($filter_name == 'posted_date_min_exact'){
            $posted_date_min = $filters['posted_date_min_exact'];
            $search_filters[] = '(posted_date>=' . $posted_date_min . ')';
        }

        else if ($filter_name == 'posted_date_max_exact'){
            $posted_date_max = $filters['posted_date_max_exact'];
            $search_filters[] = '(posted_date<=' . $posted_date_max . ')';
        }

        // sak - 061622 - topics
        else if ($filter_name == 'topics'){
            if ($filter_value){
                $this_filter = '(topics.name:"'.$filter_value.'")';
                $search_filters[] = $this_filter;
            }
        }

        // sak - 041323 - source
        else if ($filter_name == 'source'){
            if ($filter_value){
                $this_filter = '(source.name:"'.$filter_value.'")';
                $search_filters[] = $this_filter;
            }
        }

        else if ($filter_name == 'document_type'){
            if (!empty($filter_value)){
                $filter_value = explode('|', $filter_value);
                if (is_array($filter_value)){
                    $filter_string = '';
                    $i = 0;
                    foreach ($filter_value as $value){
                        if ($i>0){
                            $filter_string .= ' OR ';
                        }
                        $filter_string .= 'type.slug:'.$value;
                        $i++;
                    }
                    $filter_string = '('.$filter_string.')';
                } else {
                    $filter_string = '(type.slug:' . $filter_value . ')';
                }
                $search_filters[] = $filter_string;
            }
        }

        else if ($filter_name == 'category'){
            if (!empty($filter_value)){
                $filter_value = explode('|', $filter_value);
                if (is_array($filter_value)){
                    $filter_string = '';
                    $i = 0;
                    foreach ($filter_value as $value){
                        if ($i>0){
                            $filter_string .= ' OR ';
                        }
                        $filter_string .= 'categories.slug:'.$value;
                        $i++;
                    }
                    $filter_string = '('.$filter_string.')';
                } else {
                    $filter_string = '(categories.slug:' . $filter_value . ')';
                }
                $search_filters[] = $filter_string;
            }
        }

        else if ($filter_name == 'collection'){
            if ($filter_value){
                $this_filter = '(collections.name:"'.$filter_value.'")';
                $search_filters[] = $this_filter;
            }
        }

    }   

    if (is_array($search_filters)){

        $search_filters = implode(' AND ', $search_filters);
        
    } else {
        return null;
    }

    return $search_filters;

}

// ----------------------------------------------------------------------
// this is the primary search function to return posts

/*
    // how to build a search query:

    // default index:
    // dev_pns (sorted by document date, desc)

    // replicas:
    // dev_pns_sortby_document_date_asc (sorted by document date, desc)
    // dev_pns_sortby_posted_date_desc (sorted by posted date, desc)
    // dev_pns_sortby_posted_date_asc (sorted by posted date, asc)
    // dev_pns_sortby_title_desc (sorted by document title, desc)
    // dev_pns_sortby_title_asc (sorted by document title, asc)
    
    // you can also pass an index parameter in the query string for ajax calls to sort appropriately - it's set automatically whenever you filter via a hidden field in the sidebar search

    // The filters should be an array like this:

    $filters = array(
        'key' => 1, // 1 for featured, 0 for not (will include both non-featured and featured), 2 for *only* non-featured
        'keywords' => '', // not necessary to include if empty
        'record_type' => 'sourcedocument' // don't confuse record_type and document_type: record type should be sourcedocument (the actual post_type) - document type is a selected attribute - econrelease and reports can be filtered out using the checkbox on the form 
    );
    
    The filters array is then transformed by the build_filter_string function into a "filters" string that is passed to Algolia

    The build_filter_string is the real powerhouse behind this search and should be the first place we look for bugs

    ********************************************
    NEVER FORGET THAT PAGE IS ***ZERO-INDEXED***
    ********************************************

*/

function ajax_autocomplete() { 
    if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php')) {
        require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');
    }
    require $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
    global $wpdb;
    global $algolia;
    $client = Algolia\AlgoliaSearch\SearchClient::create(
      'xxxxxx',
      'xxxxxx'
    );
    $index = $client->initIndex('dev_pns');

    $results = $_POST['val'];
    
    echo $results;
    wp_die();
}
add_action( 'wp_ajax_autocomplete', 'ajax_autocomplete' ); 
add_action( 'wp_ajax_nopriv_autocomplete', 'ajax_autocomplete' ); 

function algolia_get_posts($limit=20, $filters=array(), $count_only=false, $pg=1, $exact = false){
    if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php')) {
        require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');
    }
    require $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
    global $wpdb;
    global $algolia;
    $client = Algolia\AlgoliaSearch\SearchClient::create(
      'xxxxxx',
      'xxxxxx'
    );
    $pg = intval($pg - 1);
    if (isset($filters['sort_order'])){
        if ($filters['sort_order']=='doc_date_desc'){
            $filters['index'] = 'dev_pns';
        } else if ($filters['sort_order'] == 'doc_date_asc'){
            $filters['index'] = 'dev_pns_sortby_document_date_asc';
        } else if ($filters['sort_order'] == 'posted_date_desc'){
            $filters['index'] = 'dev_pns_sortby_posted_date_desc';
        } else if ($filters['sort_order'] == 'posted_date_asc'){
            $filters['index'] ='dev_pns_sortby_posted_date_asc';
        }
    } else {
        $filters['index'] = 'dev_pns';
    }
    $index = $client->initIndex($filters['index']);
    
    // other single use indices to check for exact match when keyword searching
    $collections_index = $client->initIndex('dev_collections');
    $sources_index = $client->initIndex('dev_sources');
    $topics_index = $client->initIndex('dev_topics');

    $posts = array();
    if ($limit==-1){
        $limit = 9999;
    }
    if ($count_only===true){
        $get_total_count = get_total_count($filters);
        return $get_total_count;
    } else {
        // sak - 06102022 - change how the keywords are passed
        // we need to check the filters array for a keywords key - if it exists we need to remove it
        if ($exact === true){
            $keywords = '';
            if (array_key_exists('keywords', $filters)){
                $keywords = '"'.format_keywords($filters['keywords']).'"';
                // check the filter array to see if a topics filter has been passed - if so, ignore this
                // and let the selected filter to ride (since we're only allowing a single topic to be passed)
                if (!isset($filters['topics'])){
                    $get_topics = $topics_index->search('', [
                        'filters' => '(topic:'.$keywords.')',
                        'hitsPerPage' => 99,
                        'attributesToHighlight' => null
                    ]);
                    // if a hit is returned here, it means the topic exists, which means somebody typed in an exact topic name... 
                    //let's add that topic to the filter to ensure we include that topic in the results
                    if (count($get_topics['hits'])>0){
                        $filters['topics'] = format_keywords($filters['keywords']);
                    }
                }
                // check the filter array to see if a collections filter has been passed - if so, ignore this
                // and let the selected filter to ride (since we're only allowing a single collection to be passed)
                if (!isset($filters['collection'])){
                    $get_collections = $collections_index->search('', [
                        'filters' => '(collection:'.$keywords.')',
                        'hitsPerPage' => 99,
                        'attributesToHighlight' => null
                    ]);
                    // if a hit is returned here, it means the topic exists, which means somebody typed in an exact topic name... 
                    //let's add that topic to the filter to ensure we include that topic in the results
                    if (count($get_collections['hits'])>0){
                        $filters['collection'] = format_keywords($filters['keywords']);
                    }
                }
                // check the filter array to see if a collections filter has been passed - if so, ignore this
                // and let the selected filter to ride (since we're only allowing a single collection to be passed)
                if (!isset($filters['source'])){
                    $get_sources = $sources_index->search('', [
                        'filters' => '(source:'.$keywords.')',
                        'hitsPerPage' => 99,
                        'attributesToHighlight' => null
                    ]);
                    // if a hit is returned here, it means the topic exists, which means somebody typed in an exact topic name... 
                    //let's add that topic to the filter to ensure we include that topic in the results
                    if (count($get_sources['hits'])>0){
                        $filters['source'] = format_keywords($filters['keywords']);
                    }
                }
                unset($filters['keywords']);
            }
        }

        //debug($filters);

        $search_filters = build_filter_string($filters);

        if ($exact === true){

            $get_posts = $index->search($keywords, [
                'filters' => $search_filters,
                'hitsPerPage' => $limit,
                'attributesToHighlight' => null,
                'page' => $pg
            ]);

        } else {

            $get_posts = $index->search('', [
                'filters' => $search_filters,
                'hitsPerPage' => $limit,
                'attributesToHighlight' => null,
                'page' => $pg
            ]);
        }

        $posts['total_results'] = $get_posts['nbHits'];
        $posts['total_pages'] = $get_posts['nbPages'];

        $posts['posts'] = array();
        foreach($get_posts['hits'] as $post){
            $posts['posts'][] = $post;
        }

        debug($posts);
        
        return $posts;
    }
}

// ----------------------------------------------------------------------

function algolia_get_topics(){
    if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php')) {
        require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');
    }
    require $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
    global $wpdb;
    global $algolia;
    $client = Algolia\AlgoliaSearch\SearchClient::create(
      'xxxxxx',
      'xxxxxx'
    );
    $index = $client->initIndex('dev_topics');
    $get_topics = $index->search('', [
        'filters' => 'objectID:1',
        'attributesToHighlight' => null
    ]);
    return $get_topics['hits'][0]['topics'];
}


function algolia_get_post($post_id, $filters=array()){
    if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php')) {
        require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');
    }
    require $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
    global $wpdb;
    global $algolia;
    $client = Algolia\AlgoliaSearch\SearchClient::create(
      'xxxxxx',
      'xxxxxx'
    );
    $index = $client->initIndex('dev_pns');
    if (is_null($post_id)){
        // we need to check the filters array for a keywords key - if it exists we need to remove it
        if (array_key_exists('keywords', $filters)){
            $keywords = $filters['keywords'];
            //unset($filters['keywords']);
        }
        $search_filters = build_filter_string($filters);
        $post = null;
    } else {
        $post = $index->search($post_id, [
            'restrictSearchableAttributes' => [
                'post_id'
            ]
        ]);
        if (count($post['hits'])>0){ 
            $post = $post['hits'][0];
        }
    }
    return $post;
}


// ----------------------------------------------------------------------

function ajax_get_posts() { 
    if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php')) {
        require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');
    }
    require $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
    global $wpdb;

    //debug($_POST);

    if (isset($_POST['filters'])){
        // this is an extra step to take a post object and format it into an array that the build_filter_string function understands
        $search_filters = build_filters_array($_POST['filters']);
    } else {
        $search_filters = array();
    }

    if (!isset($search_filters['index'])){ $search_filters['index'] = 'dev_pns'; }

    if (!isset($search_filters['source'])){ $search_filters['source'] = false; }
    if (!isset($search_filters['document_type'])){ $search_filters['document_type'] = false; }
    if (!isset($search_filters['category'])){ $search_filters['category'] = false; }
    if (!isset($search_filters['collection'])){ $search_filters['collection'] = false; }
    if (!isset($search_filters['archive'])){ $search_filters['archive'] = false; }

    //debug('test');
    //fdebug($search_filters);
    if (!isset($search_filters['pg'])){
        $pg = 1;
    } else {
        $pg = $search_filters['pg'];
    }
    if ((isset($search_filters['boolean_method']))&&($search_filters['boolean_method']=='exact')){
        $exact = true;
    } else {
        $exact = false;
    }
    // get the results from algolia
    $results = algolia_get_posts(20, $search_filters, false, $pg, $exact);

    //debug($results);

    $results_count = $results['total_results'];  
    $content = '';
    $content .= '<header class="all-documents-header">';
        $content .= '<p class="results-count" id="num_results">';
            //$results_count = algolia_get_posts(-1, $search_filters, true);
            $content .= number_format($results_count, 0, '', ',');
            if ($results_count==1){
                $content .= ' Result';
            } else {
                $content .= ' Results';
            }
        $content .= '</p>';

        // ***
        // DISPLAY THE CLICKABLE FILTERS ON THE RESULTS
        // ***

        if (
            ((isset($search_filters['keywords']))&&(strlen($search_filters['keywords'])>0))
            || ($search_filters['doc_date_min']!=0)
            || ($search_filters['posted_date_min']!=0)
            || (strlen($search_filters['source'])>0)
            || (strlen($search_filters['document_type'])>0)
            || (strlen($search_filters['category'])>0)
            || (strlen($search_filters['collection'])>0)
            || (isset($search_filters['key']))
        ) {
            $has_filters = true;
        } else {
            $has_filters = false;
        }

        if ($has_filters===true){

            $content .= '<div class="active-filters" id="active-filters">';
                $active_filters = array();
                if ((isset($search_filters['keywords']))&&(strlen($search_filters['keywords'])>0)){
                    $active_filters[] = '<a href="javascript:void(0);" data-filter="keywords" data-filter-type="keywords">Keywords: '.removeAllSlashes($search_filters['keywords']).'</a>';
                }
                if ($search_filters['doc_date_min']!=0){
                    if ($search_filters['doc_date_min'] == $search_filters['doc_date_max']){
                        $active_filters[] = '<a href="javascript:void(0);" data-filter="doc_date_min" data-filter-type="doc_date_min">Document Date: ' . $search_filters['doc_date_min'] . '</a>';
                    } else {
                        $active_filters[] = '<a href="javascript:void(0);" data-filter="doc_date_min" data-filter-type="doc_date_min">Document Start Date: ' . $search_filters['doc_date_min'] . '</a>';
                        $active_filters[] = '<a href="javascript:void(0);" data-filter="doc_date_max" data-filter-type="doc_date_max">Document End Date: ' . $search_filters['doc_date_max'] . '</a>';
                    }
                }
                if ($search_filters['posted_date_min']!=0){
                    if ($search_filters['posted_date_min'] == $search_filters['posted_date_max']){
                        $active_filters[] = '<a href="javascript:void(0);" data-filter="doc_date_min" data-filter-type="posted_date_min">Posted Date: ' . $search_filters['posted_date_min'] . '</a>';
                    } else {
                        $active_filters[] = '<a href="javascript:void(0);" data-filter="posted_date_min" data-filter-type="posted_date_min">Posted Start Date: ' . $search_filters['posted_date_min'] . '</a>';
                        $active_filters[] = '<a href="javascript:void(0);" data-filter="posted_date_max" data-filter-type="posted_date_max">Posted End Date: ' . $search_filters['posted_date_max'] . '</a>';
                    }
                }
                if (strlen($search_filters['source'])>0){
                    if (($search_filters['archive']=='true')&&($search_filters['archive_key']=='archive_source')){

                    } else {
                        
                    }
                }
                if (isset($search_filters['document_type'])) {
                    if (strlen($search_filters['document_type'])>0){
                        if (($search_filters['archive']=='true')&&($search_filters['archive_key']=='archive_document_type')){

                        } else {
                            $active_filters[] = '<a href="javascript:void(0);" data-filter="modify-document_type-filter" data-filter-type="document_type" class="edit">Modify Document Types</a>';
                        }
                    }
                }
                if (isset($search_filters['category'])) {
                    if (strlen($search_filters['category'])>0){
                        if (($search_filters['archive']=='true')&&($search_filters['archive_key']=='archive_category')){
                        
                        } else {
                            $active_filters[] = '<a href="javascript:void(0);" data-filter="modify-category-filter" data-filter-type="category" class="edit">Modify Categories</a>';
                        }
                    }
                }
                if (isset($search_filters['collection'])) {
                    if (strlen($search_filters['collection'])>0){
                        if (($search_filters['archive']=='true')&&($search_filters['archive_key']=='archive_collection')){

                        } else {
                            
                        }
                    }
                }
                if (isset($search_filters['key'])){
                    if ($search_filters['key']==1){
                        $active_filters[] = '<a href="javascript:void(0);" data-filter="key" data-filter-type="key">Show Key Documents Only</a>';
                    }
                }    
                $content .= implode(' ', $active_filters);
            $content .= '</div>';
            
            $content .= '<div class="active-filters-submit-wrapper"><button class="button" type="button" id="search-filters-submit">Search <span></span></button></div>';

        }

    $content .= '</header>';
    $c = 0;
    $posts = $results['posts'];
    $total_pages = $results['total_pages'];

    $pagination = '';
    $pagination .= '<nav class="pagination">';
        // don't forget that algolia pagination (i.e. 'page') is ZERO-based!!!
        if (($posts!==false)&&(!empty($posts))&&(intval(str_replace(array(',','.'), array('', ''), $results_count))>20)){
            $tmp = [];
            for($p = 1; $p <= $total_pages; $p++) {
                if($pg == $p) {
                    $tmp[] = "<strong>{$p}</strong>";
                } else {
                    $tmp[] = '<a href="/all-source-documents/?pg='.$p.'" data-attr-load-page="'.($p).'" >'.$p.'</a>';
                }
            }
            for($i = count($tmp) - 3; $i > 1; $i--) {
                if(abs($pg - $i - 1) > 2) {
                    unset($tmp[$i]);
                }
            }
            if(count($tmp) > 1) {
                $pagination .= '<ul>';
                    if($pg > 1) {
                        $pagination .= '<li class="prev"><a href="/all-source-documents/?pg='.($pg-1).'" data-attr-load-page="'.($pg-1).'">&lt; Prev</a></li>';
                    }
                    $lastlink = 0;
                    foreach($tmp as $i => $link) {
                        if($i > $lastlink + 1) {
                            $pagination .= "<li> ... </li>";
                        }
                        $pagination .= '<li>'.$link.'</li>';
                        $lastlink = $i;
                    }
                    if($pg < $total_pages) {
                        //debug($pg);
                        //debug($total_pages);
                        $pagination .= '<li class="next"><a href="/all-source-documents/?pg='.($pg+1).'" data-attr-load-page="'.($pg+1).'">Next &gt;</a></li>';
                    }
                $pagination .= '</ul>';
                $pagination .= '<div class="gotopage"><input type="number" name="gotopage" id="gotopage" placeholder="1" min="1" max="'.$total_pages.'"><button class="button"><span class="">Jump to page</a></button></div>';
            }
        } 
    $pagination .= '</nav>';

    $content .= '<div class="table-wrapper">';
        $content .= $pagination; // top pagination 
        $content .= '<table class="documents algolia-helpers" width="100%" id="documents">';
            $content .= '<thead>';
                $content .= '<tr>';
                    $content .= '<th scope="col" class="favorite" width="5%"></th>';
                    $content .= '<th scope="col" width="50%"><span>Source</span></th>';
                    $content .= '<th scope="col" width="17%" class="sortable" data-sort-order="';
                    if ($search_filters['sort_order']=='doc_date_desc'){
                        $content .= 'desc';
                    } else {
                        $content .= 'asc';
                    }
                    $content .= '" data-sort-active="';
                    if ($search_filters['sort_order']=='doc_date_desc'||$search_filters['sort_order']=='doc_date_asc'){
                        $content .= 'true';
                    } else {
                        $content .= 'false';
                    }
                    $content .= '" data-sort-key="document_date" colspan="2"><span>Document Date</span></th>';
                    //$content .= '<th scope="col" width="17%" class="sortable" data-sort-order="';
                    //if ($search_filters['sort_order']=='posted_date_desc'){
                    //    $content .= 'desc';
                    //} else {
                    //    $content .= 'asc';
                    //}
                    //$content .= '" data-sort-active="';
                    //if ($search_filters['sort_order']=='posted_date_desc'||$search_filters['sort_order']=='posted_date_asc'){
                   //     $content .= 'true';
                    //} else {
                    //    $content .= 'false';
                    //}
                    //$content .= '" data-sort-key="posted_date"><span>Posted Date</span></th>';
                    //$content .= '<th scope="col" width="12%">Identifier</th>';
                    $content .= '<th scope="col" width="28%" colspan="2">Document Type</th>';
                   // $content .= '<th scope="col" width="9%" class="download">Download</th>';
                $content .= '</tr>';
            $content .= '</thead>';
            $content .= '<tbody data-attr-total-count="'.intval($results_count).'" data-attr-current-count="" >';
            if (($posts!==false)&&(!empty($posts))){
                //if (isset($search_filters['index'])) {
                if (($search_filters['index']=='dev_pns')||($search_filters['index']=='dev_pns_sortby_document_date_asc')){
                    $grouped_posts = array();
                    foreach ($posts as $post){
                        $year = $post['year'];
                        $month = $post['month'];
                        $grouped_posts[$year][$month][] = $post;
                    }
                    foreach ($grouped_posts as $year => $months){
                        foreach ($months as $month => $posts){
                            if ($c > 0){
                                $content .= '<tr class="grouped-header d"><td></td><td colspan="6">';
                                    $content .= '<h2><span>'.date('F Y', strtotime($month.'/1/'.$year)).'</span></h2>';
                                $content .= '</td></tr>';
                                $content .= '<tr class="grouped-header m"><td colspan="7">';
                                    $content .= '<h2><span>'.date('F Y', strtotime($month.'/1/'.$year)).'</span></h2>';
                                $content .= '</td></tr>';
                            }
                            foreach ($posts as $post){
                                $content .= algolia_document_table_row($post, $year, $month);
                            }
                            $c++;
                        }
                    }
                } else {
                    foreach ($posts as $post){ 
                        $content .= algolia_document_table_row($post);
                    } 
                }
                //}
            } else {
                $content .= '<tr><td colspan="6" class="no-docs-found"><p>No documents found.</p></td></tr>';
            }
            $content .= '</tbody>';
        $content .= '</table>';

        $content .= $pagination;
        //$content .= "<script>$(\".all-documents-header\").prepend('".$pagination."');</script>";
        //use jQuery to put pagination at top also
    $content .= '</div>';
    echo $content;
    wp_die();
}
add_action( 'wp_ajax_get_posts', 'ajax_get_posts' ); 
add_action( 'wp_ajax_nopriv_get_posts', 'ajax_get_posts' ); 

// ----------------------------------------------------------------------

function get_child_terms($parent_term, $taxonomy){
    global $wpdb;
    $child_terms = array();
    $parent = get_term_by('slug', $parent_term, $taxonomy);
    $parent_id = $parent->term_id;
    $children = get_term_children($parent_id, $taxonomy);
    $where = array();
    if ($children){
        foreach ($children as $key => $child_id){
            $where[] = " ( term_id = '" . $child_id . "' ) ";
        }
        $where = implode(" OR ", $where);
        $query = " select slug from wp_terms where ( " . $where . " )";
        $get_slugs = $wpdb->get_results($query);
        foreach ($get_slugs as $slug){
            $child_terms[] = $slug->slug;
        }
    }
    return $child_terms;
}

// ----------------------------------------------------------------------

function build_filters_url_string($filters){
    $search_filters = implode('&', $filters);
}

function build_filters_array($filters){
    $search_filters = array();
    $filters = explode('&', $filters);

    foreach ($filters as $filter){
        $this_filter = explode('=', $filter);
        $this_filter_key = $this_filter[0];
        if ($this_filter_key == 'keywords'){
            $this_filter_value = format_keywords($this_filter[1]);
        }
        if ($this_filter_key == 'topics'){
            $this_filter_value = format_keywords($this_filter[1]);
        }
        if ($this_filter_key == 'collection'){
            $this_filter_value = format_keywords($this_filter[1]);
        }
        if ($this_filter_key == 'source'){
            $this_filter_value = format_keywords($this_filter[1]);
        }
        else if ($this_filter_key == 'boolean_method'){
            $this_filter_value = $this_filter[1];
        }
        else if ($this_filter_key == 'pg'){
            $this_filter_value = $this_filter[1];
        }
        else if ($this_filter_key == 'doc_date_min'){
            $this_filter_value = $this_filter[1];
        }
        else if ($this_filter_key == 'doc_date_max'){
            $this_filter_value = $this_filter[1];
        }
        else if ($this_filter_key == 'posted_date_min'){
            $this_filter_value = $this_filter[1];
        }
        else if ($this_filter_key == 'posted_date_max'){
            $this_filter_value = $this_filter[1];
        }
        // else if ($this_filter_key == 'source'){
        //     $this_filter_value = $this_filter[1];
        // }
        else if ($this_filter_key == 'document_type'){
            $this_filter_value = $this_filter[1];
        }
        else if ($this_filter_key == 'category'){
            $this_filter_value = $this_filter[1];
        }
        // else if ($this_filter_key == 'collection'){
        //     $this_filter_value = $this_filter[1];
        // }
        else if ($this_filter_key == 'key'){
            $this_filter_value = '1';
        }
        else if ($this_filter_key == 'sort_order'){
            $this_filter_value = $this_filter[1];
        }
        else if ($this_filter_key == 'archive'){
            $this_filter_value = 'true';
        }
        else if ($this_filter_key == 'archive_key'){
            $this_filter_value = $this_filter[1];
        }
        $search_filters[$this_filter_key] = $this_filter_value;
    }
    return $search_filters;
}

// ----------------------------------------------------------------------

function format_keywords($keywords){
    $keywords = html_entity_decode($keywords);
    $keywords = urldecode($keywords);
    $keywords = trim($keywords);
    return $keywords;
}

// ----------------------------------------------------------------------

function cleanSlug($slug) {
   $slug = str_replace(' ', '-', $slug);
   $slug = preg_replace('/[^A-Za-z0-9\-]/', '', $slug);
   return preg_replace('/-+/', '-', $slug);
}

function cleanString($string, $autop=true) {
    if ($autop===true){
        return wpautop($string);
    }
}

// ----------------------------------------------------------------------

function get_doc_table_header($params=array(), $sortable=true){    


    //debug($params);

    echo '<thead>';
        echo '<tr>';
            echo '<th scope="col" class="favorite" width="5%"></th>';
            echo '<th scope="col" width="50%"><span>Source</span></th>';
            echo '<th scope="col" width="17%"';
            if ($sortable===true){
                echo ' class="sortable" data-sort-order="desc" data-sort-active="';
                if ((isset($params['index']))&&(($params['index']=='dev_pns')||($params['index']=='dev_pns_sortby_document_date_asc'))){
                    echo 'true';
                } else {
                    if (!isset($_GET['index'])){
                        echo 'true';
                    } else {
                        echo 'false';
                    }
                }
                echo '" data-sort-key="document_date"';
            }
            echo ' colspan="2"><span>Document Date</span></th>';
            //echo '<th scope="col" width="17%"';
            //if ($sortable===true){
            //    echo ' class="sortable" data-sort-order="desc" data-sort-active="';
            //    if ((isset($params['index']))&&(($params['index']=='dev_pns_sortby_posted_date_desc')||($params['index']=='dev_pns_sortby_posted_date_asc'))){
            //        echo 'true';
            //    } else {
            //        echo 'false';
            //    }
            //    echo '" data-sort-key="posted_date"';
            //}
            //echo '><span>Posted Date</span></th>';
            //echo '<th scope="col" width="12%">Identifier</th>';
            echo '<th scope="col" width="28%" colspan="2">Document Type</th>'; 
            //echo '<th scope="col" width="9%" class="download">Download</th>';
        echo '</tr>';
    echo '</thead>';
}

// ----------------------------------------------------------------------

function limit_text($text, $limit) {
    if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos   = array_keys($words);
        $text  = substr($text, 0, $pos[$limit]) . '...';
    }
    return $text;
}

// added 4/6/2023 - function used to order the category list alphabetically
function compareByName($a, $b) {
  return strcmp($a["name"], $b["name"]);
}

function algolia_document_table_row($post, $row_group_year='', $row_group_month='', $limit_excerpt_length=30){

    // IMPORTANT TO KEEP IN MIND HERE THAT THERE ARE NOW *** 6 *** TOTAL COLUMNS
    // THE MOBILE VIEW AND DESKTOP VIEWS ARE ALL TOGETHER FOR REASONS MUCH MORE
    // COMPLEX TO DESCRIBE HERE, SO EACH ROW NEEDS TO ACCOUNT FOR THESE 6 COLUMNS
    // *** IF YOU REMOVE OR ADD A COLUMN, BE SURE TO:
    // 1. ADJUST COLSPAN VALUES (MAKE SURE EACH ROW ADDS UP TO THE 6 COLUMNS)
    // 2. ADJUST WIDTHS IN THE TABLE HEADER (LOCATED NOW IN ALGOLIA_GET_POSTS)
    // 3. ADJUST WIDTHS IN THE CSS

    $is_featured = false;
    if ($post['key']==1){
        $is_featured = true;
    }
    $row_content = '';
    $document_date =  date("m/d/Y", $post['document_date']);   
    $display_date_as = $post['document_date_display'];
    if ($display_date_as == 'my'){
        $date_display = date("m/Y", $post['document_date']);
    } elseif ($display_date_as == 'y'){
        $date_display = date("Y", $post['document_date']);
    } else {
        $date_display = date("m/d/Y", $post['document_date']);
    }
    $posted_date = date("m/d/Y", $post['posted_date']);
    $permalink = '/'.$post['record_type'].'/'.$post['slug'].'/';
    $title = $post['title'];
    $excerpt = cleanString($post['excerpt']);

    /*
        sak - 06062022 - BH #121 - 3 lines
        limit the excerpt here
        defaults to 30 - pass overrides in the function call if necessary
    */
    if (($limit_excerpt_length!==false)&&(is_int($limit_excerpt_length))){
        $excerpt = limit_text($excerpt, $limit_excerpt_length);
    }

    $source_terms = $post['source'];
    $source_term_list = '';
    $source_term_slug = '';
    $i = 0;
    foreach ($source_terms as $source_term){
        if ($i > 0){
            $source_term_list .= ', ';
        }
        $source_term_list .= $source_term['name'];
        $source_term_slug = $source_term['slug'];
        $i++;
    }
    $type_terms = $post['type'];
    $type_term_list = '';
    $i = 0;
    foreach ($type_terms as $type_term){
        if ($i > 0){
            $type_term_list .= ', ';
        }
        $type_term_list .= $type_term['name'];
        $i++;
    }
    $collection_terms = $post['collections'];
    $collection_term_list = '<span class="tag collection">';
    foreach ($collection_terms as $collection_term){
        $collection_term_list .= '<a href="/collection/'.$collection_term['slug'].'">'.$collection_term['name'].'</a>';
    }
    $collection_term_list .= '</span>';
    $category_terms = $post['categories'];
    $category_term_list = '<span class="tag category">';

    // added 4/6/2023 - order the category list alphabetically
    usort($category_terms, 'compareByName');

    foreach ($category_terms as $category_term){
        $category_term_list .= '<a href="/newscategory/'.$category_term['slug'].'">'.$category_term['name'].'</a>';
    }
    $category_term_list .= '</span>';

    $row_content .= '<tr class="post-first-row m';
        if ($is_featured===true){
            $row_content .= ' featured';
        }
    $row_content .= '"';
    if ($row_group_year!=''){
        $row_content .= ' data-attr-year="' . $row_group_year . '"';
    }
    if ($row_group_month!=''){
        $row_content .= ' data-attr-month="' . $row_group_month . '"';
    }
    $row_content .= '>';
        $row_content .= '<td class="primary-source" colspan="5">';
            $row_content .= '<a href="/primarysource/'.$source_term_slug.'">'.$source_term_list.'</a>';
        $row_content .= '</td>';
        $row_content .= '<td class="favorite">';
            $row_content .= '<div class="stars">';
                $row_content .= '<span class="star star-o"><i class="fa fa-solid fa-key" aria-hidden="true"></i></span>';
            $row_content .= '</div>';
        $row_content .= '</td>';
    $row_content .= '</tr>';

    $row_content .= '<tr class="m"';
    if ($row_group_year!=''){
        $row_content .= ' data-attr-year="' . $row_group_year . '"';
    }
    if ($row_group_month!=''){
        $row_content .= ' data-attr-month="' . $row_group_month . '"';
    }
    $row_content .= '>';
        $row_content .= '<td colspan="6">';
            $row_content .= '<h3><a href="'.$permalink.'">'.$title.'</a></h3>';
            $row_content .= $excerpt;
        $row_content .= '</td>';
    $row_content .= '</tr>';
    $row_content .= '<tr class="m m-last-row"';
    if ($row_group_year!=''){
        $row_content .= ' data-attr-year="' . $row_group_year . '"';
    }
    if ($row_group_month!=''){
        $row_content .= ' data-attr-month="' . $row_group_month . '"';
    }
    $row_content .= '>';
        $row_content .= '<td colspan="2">';
            $row_content .= '<table class="details" border="1">';
                $row_content .= '<tbody>';
                    $row_content .= '<tr>';
                        $row_content .= '<td class="date">';
                            $row_content .= $date_display;
                        $row_content .= '</td>';
                    $row_content .= '</tr>';
                    //$row_content .= '<tr>';
                    //    $row_content .= '<td class="date">';
                    //        $row_content .= $posted_date;
                    //    $row_content .= '</td>';
                    //$row_content .= '</tr>';
                    //$row_content .= '<tr>';
                    //    $row_content .= '<td class="identifier">';
                    //        $row_content .= $post['doc_id'];
                    //    $row_content .= '</td>';
                    //$row_content .= '</tr>';
                    $row_content .= '<tr>';
                        $row_content .= '<td class="doctype">';
                            $row_content .= $type_term_list;
                        $row_content .= '</td>';
                    $row_content .= '</tr>';
                $row_content .= '</tbody>';
            $row_content .= '</table>';
        $row_content .= '</td>';
        $row_content .= '<td class="doc-tags" colspan="4"  valign="bottom">';
            $row_content .= '<div class="category-outer-wrapper">';
                $row_content .= '<div class="category-inner-wrapper">';
                    //$row_content .= '<a href="#" class="download m">Download <i></i></a>';
                    $row_content .= $category_term_list;
                    $row_content .= $collection_term_list;
                $row_content .= '</div>';
            $row_content .= '</div>';
        $row_content .= '</td>';
    $row_content .= '</tr>';
    $row_content .= '<tr class="m m-sep"';
    if ($row_group_year!=''){
        $row_content .= ' data-attr-year="' . $row_group_year . '"';
    }
    if ($row_group_month!=''){
        $row_content .= ' data-attr-month="' . $row_group_month . '"';
    }
    $row_content .= '><td colspan="6"></td></tr>';
    $row_content .= '<tr class="view detail-row d"';
    if ($row_group_year!=''){
        $row_content .= ' data-attr-year="' . $row_group_year . '"';
    }
    if ($row_group_month!=''){
        $row_content .= ' data-attr-month="' . $row_group_month . '"';
    }
    $row_content .= '>';
        $row_content .= '<td class="favorite"></td>';
        $row_content .= '<td class="primary-source">';
            $row_content .= '<a href="/primarysource/'.$source_term_slug.'">'.$source_term_list.'</a>';
        $row_content .= '</td>';
        $row_content .= '<td class="date" colspan="2">';
            $row_content .= $date_display;
        $row_content .= '</td>';
        //$row_content .= '<td class="date">';
        //    $row_content .= $posted_date;
        //$row_content .= '</td>';
        //$row_content .= '<td class="identifier">';
        //    $row_content .= $post['doc_id'];
        //$row_content .= '</td>';
        $row_content .= '<td class="doctype" colspan="2">';
            $row_content .= $type_term_list;
        $row_content .= '</td>';
        //$row_content .= '<td class="download"><a href="#"><span class="acc">Download</span><i></i></a></td>';
    $row_content .= '</tr>';
    $row_content .= '<tr class="view content-row d';
        if ($is_featured===true){
            $row_content .= ' featured';
        }
    $row_content .= '"';
    if ($row_group_year!=''){
        $row_content .= ' data-attr-year="' . $row_group_year . '"';
    }
    if ($row_group_month!=''){
        $row_content .= ' data-attr-month="' . $row_group_month . '"';
    }
    $row_content .= '>';
    $row_content .= '<td class="favorite">';
        $row_content .= '<div class="stars">';
            $row_content .= '<span class="star star-o"><i class="fa  fa-solid fa-key" aria-hidden="true"></i></span>';
        $row_content .= '</div>';
    $row_content .= '</td>';
    if (is_front_page()){
        $row_content .= '<td colspan="2">';
            $row_content .= '<h3><a href="'.$permalink.'">'.$title.'</a></h3>';
        $row_content .= '</td>';
        $row_content .= '<td colspan="3">';
            $row_content .= '';
        $row_content .= '</td>';
    } else {
        $row_content .= '<td colspan="6">';
            $row_content .= '<h3><a href="'.$permalink.'">'.$title.'</a></h3>';
        $row_content .= '</td>';
    }
    $row_content .= '</tr>';
    $row_content .= '<tr class="last view content-row d"';
    if ($row_group_year!=''){
        $row_content .= ' data-attr-year="' . $row_group_year . '"';
    }
    if ($row_group_month!=''){
        $row_content .= ' data-attr-month="' . $row_group_month . '"';
    }
    $row_content .= '>';
        $row_content .= '<td class="favorite"></td>';
        $row_content .= '<td colspan="3" class="doc-excerpt">';
            $row_content .= $excerpt;
        $row_content .= '</td>';
        $row_content .= '<td colspan="2" class="doc-tags">';
            $row_content .= '<div class="category-outer-wrapper">';
                $row_content .= '<div class="category-inner-wrapper">';
                    $row_content .= $category_term_list;
                    $row_content .= $collection_term_list;
                $row_content .= '</div>';
            $row_content .= '</div>';
        $row_content .= '</td>';
    $row_content .= '</tr>';


    return $row_content;
}                    
// ----------------------------------------------------------------------

function algolia_document_d3record($post, $row_group_year='', $row_group_month=''){
    
}

// ----------------------------------------------------------------------
// get data for filter dropdowns

function get_search_categories($id=null, $returnFormOptions=false){
    if (is_null($id)){
        // get them all
        $categories = array();
        global $wpdb;
        $query = $wpdb->get_results("select wp_terms.term_id, wp_terms.name, wp_terms.slug from wp_terms, wp_term_taxonomy where wp_term_taxonomy.taxonomy = 'newscategory' and wp_term_taxonomy.parent = '0' and wp_term_taxonomy.term_id = wp_terms.term_id order by wp_terms.term_order ASC");
        foreach ($query as $category){

            // get child sources
            $children = $wpdb->get_results("select wp_terms.term_id, wp_terms.name, wp_terms.slug from wp_terms, wp_term_taxonomy where wp_term_taxonomy.taxonomy = 'newscategory' and wp_term_taxonomy.parent = '".$category->term_id."' and wp_term_taxonomy.term_id = wp_terms.term_id order by wp_terms.term_order ASC");
            if ($children){
                $child_sources = array(); // level 2
                foreach ($children as $key => $child){
                    $child_sources[] = array(
                        'name' => $child->name,
                        'slug' => $child->slug,
                        'id' => $child->term_id
                    );
                    $grandchildren = $wpdb->get_results("select wp_terms.term_id, wp_terms.name, wp_terms.slug from wp_terms, wp_term_taxonomy where wp_term_taxonomy.taxonomy = 'newscategory' and wp_term_taxonomy.parent = '".$child->term_id."' and wp_term_taxonomy.term_id = wp_terms.term_id order by wp_terms.name ASC");
                    if ($grandchildren){
                        foreach ($grandchildren as $grandchild_key => $grandchild){
                            $child_sources[$key]['grandchildren'][] = array(
                                'name' => $grandchild->name,
                                'slug' => $grandchild->slug,
                                'id' => $grandchild->term_id
                            );
                            $great_grandchildren = $wpdb->get_results("select wp_terms.term_id, wp_terms.name, wp_terms.slug from wp_terms, wp_term_taxonomy where wp_term_taxonomy.taxonomy = 'newscategory' and wp_term_taxonomy.parent = '".$grandchild->term_id."' and wp_term_taxonomy.term_id = wp_terms.term_id order by wp_terms.name ASC");
                            if ($great_grandchildren){
                                foreach ($great_grandchildren as $great_grandchild_key => $great_grandchild){
                                    $child_sources[$key]['grandchildren'][$grandchild_key]['great_grandchildren'][] = array(
                                        'name' => $great_grandchild->name,
                                        'slug' => $great_grandchild->slug,
                                        'id' => $great_grandchild->term_id
                                    );
                                    $great_great_grandchildren = $wpdb->get_results("select wp_terms.term_id, wp_terms.name, wp_terms.slug from wp_terms, wp_term_taxonomy where wp_term_taxonomy.taxonomy = 'newscategory' and wp_term_taxonomy.parent = '".$great_grandchild->term_id."' and wp_term_taxonomy.term_id = wp_terms.term_id order by wp_terms.name ASC");
                                    if ($great_great_grandchildren){
                                        foreach ($great_great_grandchildren as $great_great_grandchild_key => $great_great_grandchild){
                                            $child_sources[$key]['grandchildren'][$grandchild_key]['great_grandchildren'][$great_grandchild_key]['great_great_grandchildren'][] = array(
                                                'name' => $great_great_grandchild->name,
                                                'slug' => $great_great_grandchild->slug,
                                                'id' => $great_great_grandchild->term_id
                                            );
                                            $great_great_great_grandchildren = $wpdb->get_results("select wp_terms.term_id, wp_terms.name, wp_terms.slug from wp_terms, wp_term_taxonomy where wp_term_taxonomy.taxonomy = 'newscategory' and wp_term_taxonomy.parent = '".$great_great_grandchild->term_id."' and wp_term_taxonomy.term_id = wp_terms.term_id order by wp_terms.name ASC");
                                            if ($great_great_great_grandchildren){
                                                foreach ($great_great_great_grandchildren as $great_great_great_grandchild_key => $great_great_great_grandchild){
                                                    $child_sources[$key]['grandchildren'][$grandchild_key]['great_grandchildren'][$great_grandchild_key]['great_great_grandchildren'][$great_great_grandchild_key]['great_great_great_grandchildren'][] = array(
                                                        'name' => $great_great_great_grandchild->name,
                                                        'slug' => $great_great_great_grandchild->slug,
                                                        'id' => $great_great_great_grandchild->term_id
                                                    );
                                                    // we can add more decendents here if needed, but geez.
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                $categories[] = array(
                    'name' => $category->name,
                    'slug' => $category->slug,
                    'id' => $category->term_id,
                    'children' => $child_sources
                );
            } else {
                $categories[] = array(
                    'name' => $category->name,
                    'slug' => $category->slug,
                    'id' => $category->term_id
                );
            }
        }
        if ($returnFormOptions===true){
            $options = '';
            foreach ($categories as $key => $category){
                $options .= '<option value="'. $category['slug'] . '">'. $category['name'] .'</option>';
                if (
                    (isset($category['children']))
                    && (is_array($category['children']))
                    && (count($category['children'])>0)
                ){
                    foreach ($category['children'] as $child_key => $child_source){
                        $options .= '<option value="'. $child_source['slug'] . '">&nbsp;&ndash;&nbsp;'. $child_source['name'] .'</option>';
                        if ((isset($child_source['grandchildren']))&&(count($child_source['grandchildren'])>0)){
                            foreach ($child_source['grandchildren'] as $grandchildren_key => $grandchild_source){
                                $options .= '<option value="'. $grandchild_source['slug'] . '">&nbsp;&ndash;&nbsp;&ndash;&nbsp;'. $grandchild_source['name'] .'</option>';
                                if ((isset($grandchild_source['great_grandchildren']))&&(count($grandchild_source['great_grandchildren'])>0)){
                                    foreach ($grandchild_source['great_grandchildren'] as $great_grandchildren_key => $great_grandchild_source){
                                        $options .= '<option value="'. $great_grandchild_source['slug'] . '">&nbsp;&ndash;&nbsp;&ndash;&nbsp;&ndash;&nbsp;'. $great_grandchild_source['name'] .'</option>';
                                        if ((isset($great_grandchild_source['great_great_grandchildren']))&&(count($great_grandchild_source['great_great_grandchildren'])>0)){
                                            foreach ($great_grandchild_source['great_great_grandchildren'] as $great_great_grandchildren_key => $great_great_grandchild_source){
                                                $options .= '<option value="'. $great_great_grandchild_source['slug'] . '">&nbsp;&ndash;&nbsp;&ndash;&nbsp;&ndash;&nbsp;&ndash;&nbsp;'. $great_great_grandchild_source['name'] .'</option>';
                                                if ((isset($great_great_grandchild_source['great_great_great_grandchildren']))&&(count($great_great_grandchild_source['great_great_great_grandchildren'])>0)){
                                                    foreach ($great_great_grandchild_source['great_great_great_grandchildren'] as $great_great_great_grandchildren_key => $great_great_great_grandchild_source){
                                                        $options .= '<option value="'. $great_great_great_grandchild_source['slug'] . '">&nbsp;&ndash;&nbsp;&ndash;&nbsp;&ndash;&nbsp;&ndash;&nbsp;&ndash;&nbsp;'. $great_great_great_grandchild_source['name'] .'</option>';
                                                        
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return $options;
        } else {
            return $categories;
        }
    }
}

function get_search_document_types($id=null, $returnFormOptions=false){
    if (is_null($id)){
        // get them all
        $document_types = array();
        global $wpdb;
        $query = $wpdb->get_results("select wp_terms.term_id, wp_terms.name, wp_terms.slug from wp_terms, wp_term_taxonomy where wp_term_taxonomy.taxonomy = 'documenttype' and wp_term_taxonomy.parent = '0' and wp_term_taxonomy.term_id = wp_terms.term_id order by wp_terms.term_order ASC");
        foreach ($query as $document_type){

           $children = $wpdb->get_results("select wp_terms.term_id, wp_terms.name, wp_terms.slug from wp_terms, wp_term_taxonomy where wp_term_taxonomy.taxonomy = 'documenttype' and wp_term_taxonomy.parent = '".$document_type->term_id."' and wp_term_taxonomy.term_id = wp_terms.term_id order by wp_terms.term_order ASC");
            if ($children){
                $child_sources = array(); // level 2
                foreach ($children as $key => $child){
                    $child_sources[] = array(
                        'name' => $child->name,
                        'slug' => $child->slug,
                        'id' => $child->term_id
                    );
                    $grandchildren = $wpdb->get_results("select wp_terms.term_id, wp_terms.name, wp_terms.slug from wp_terms, wp_term_taxonomy where wp_term_taxonomy.taxonomy = 'documenttype' and wp_term_taxonomy.parent = '".$child->term_id."' and wp_term_taxonomy.term_id = wp_terms.term_id order by wp_terms.name ASC");
                    if ($grandchildren){
                        foreach ($grandchildren as $grandchild_key => $grandchild){
                            $child_sources[$key]['grandchildren'][] = array(
                                'name' => $grandchild->name,
                                'slug' => $grandchild->slug,
                                'id' => $grandchild->term_id
                            );
                            $great_grandchildren = $wpdb->get_results("select wp_terms.term_id, wp_terms.name, wp_terms.slug from wp_terms, wp_term_taxonomy where wp_term_taxonomy.taxonomy = 'documenttype' and wp_term_taxonomy.parent = '".$grandchild->term_id."' and wp_term_taxonomy.term_id = wp_terms.term_id order by wp_terms.name ASC");
                            if ($great_grandchildren){
                                foreach ($great_grandchildren as $great_grandchild_key => $great_grandchild){
                                    $child_sources[$key]['grandchildren'][$grandchild_key]['great_grandchildren'][] = array(
                                        'name' => $great_grandchild->name,
                                        'slug' => $great_grandchild->slug,
                                        'id' => $great_grandchild->term_id
                                    );
                                    $great_great_grandchildren = $wpdb->get_results("select wp_terms.term_id, wp_terms.name, wp_terms.slug from wp_terms, wp_term_taxonomy where wp_term_taxonomy.taxonomy = 'documenttype' and wp_term_taxonomy.parent = '".$great_grandchild->term_id."' and wp_term_taxonomy.term_id = wp_terms.term_id order by wp_terms.name ASC");
                                    if ($great_great_grandchildren){
                                        foreach ($great_great_grandchildren as $great_great_grandchild_key => $great_great_grandchild){
                                            $child_sources[$key]['grandchildren'][$grandchild_key]['great_grandchildren'][$great_grandchild_key]['great_great_grandchildren'][] = array(
                                                'name' => $great_great_grandchild->name,
                                                'slug' => $great_great_grandchild->slug,
                                                'id' => $great_great_grandchild->term_id
                                            );
                                            $great_great_great_grandchildren = $wpdb->get_results("select wp_terms.term_id, wp_terms.name, wp_terms.slug from wp_terms, wp_term_taxonomy where wp_term_taxonomy.taxonomy = 'documenttype' and wp_term_taxonomy.parent = '".$great_great_grandchild->term_id."' and wp_term_taxonomy.term_id = wp_terms.term_id order by wp_terms.name ASC");
                                            if ($great_great_great_grandchildren){
                                                foreach ($great_great_great_grandchildren as $great_great_great_grandchild_key => $great_great_great_grandchild){
                                                    $child_sources[$key]['grandchildren'][$grandchild_key]['great_grandchildren'][$great_grandchild_key]['great_great_grandchildren'][$great_great_grandchild_key]['great_great_great_grandchildren'][] = array(
                                                        'name' => $great_great_great_grandchild->name,
                                                        'slug' => $great_great_great_grandchild->slug,
                                                        'id' => $great_great_great_grandchild->term_id
                                                    );
                                                    // we can add more decendents here if needed, but geez.
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                $document_types[] = array(
                    'name' => $document_type->name,
                    'slug' => $document_type->slug,
                    'id' => $document_type->term_id,
                    'children' => $child_sources
                );
            } else {
                $document_types[] = array(
                    'name' => $document_type->name,
                    'slug' => $document_type->slug,
                    'id' => $document_type->term_id
                );
            }

        }
        if ($returnFormOptions===true){
            $options = '';
            foreach ($document_types as $key => $document_type){
                $options .= '<option value="'. $document_type['slug'] . '">'. $document_type['name'] .'</option>';
                if (
                    (isset($document_type['children']))
                    && (is_array($document_type['children']))
                    && (count($document_type['children'])>0)
                ){
                    foreach ($document_type['children'] as $child_key => $child_source){
                        $options .= '<option value="'. $child_source['slug'] . '">&nbsp;&ndash;&nbsp;'. $child_source['name'] .'</option>';
                        if ((isset($child_source['grandchildren']))&&(count($child_source['grandchildren'])>0)){
                            foreach ($child_source['grandchildren'] as $grandchildren_key => $grandchild_source){
                                $options .= '<option value="'. $grandchild_source['slug'] . '">&nbsp;&ndash;&nbsp;&ndash;&nbsp;'. $grandchild_source['name'] .'</option>';
                                if ((isset($grandchild_source['great_grandchildren']))&&(count($grandchild_source['great_grandchildren'])>0)){
                                    foreach ($grandchild_source['great_grandchildren'] as $great_grandchildren_key => $great_grandchild_source){
                                        $options .= '<option value="'. $great_grandchild_source['slug'] . '">&nbsp;&ndash;&nbsp;&ndash;&nbsp;&ndash;&nbsp;'. $great_grandchild_source['name'] .'</option>';
                                        if ((isset($great_grandchild_source['great_great_grandchildren']))&&(count($great_grandchild_source['great_great_grandchildren'])>0)){
                                            foreach ($great_grandchild_source['great_great_grandchildren'] as $great_great_grandchildren_key => $great_great_grandchild_source){
                                                $options .= '<option value="'. $great_great_grandchild_source['slug'] . '">&nbsp;&ndash;&nbsp;&ndash;&nbsp;&ndash;&nbsp;&ndash;&nbsp;'. $great_great_grandchild_source['name'] .'</option>';
                                                if ((isset($great_great_grandchild_source['great_great_great_grandchildren']))&&(count($great_great_grandchild_source['great_great_great_grandchildren'])>0)){
                                                    foreach ($great_great_grandchild_source['great_great_great_grandchildren'] as $great_great_great_grandchildren_key => $great_great_great_grandchild_source){
                                                        $options .= '<option value="'. $great_great_great_grandchild_source['slug'] . '">&nbsp;&ndash;&nbsp;&ndash;&nbsp;&ndash;&nbsp;&ndash;&nbsp;&ndash;&nbsp;'. $great_great_great_grandchild_source['name'] .'</option>';
                                                        
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return $options;
        } else {
            return $document_types;
        }
    }
}

function get_total_count($filters=array(), $index='dev_pns'){
    if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php')) {
        require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');
    }
    require $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
    global $wpdb;
    global $algolia;
    $client = Algolia\AlgoliaSearch\SearchClient::create(
      'xxxxxx',
      'xxxxxx'
    );
    $index = $client->initIndex($index);
    if (!empty($filters)){
        if (array_key_exists('keywords', $filters)){
            $keywords = $filters['keywords'];
            unset($filters['keywords']);
        } else {
            $keywords = '';
        }
        $search_filters = build_filter_string($filters);

        //debug($search_filters);

        $get_posts = $index->search($keywords, [
            'filters' => $search_filters,
            'attributesToRetrieve' => null,
            'attributesToHighlight' => null,
            'hitsPerPage' => 1000,
            'analytics' => false
        ]);
    } else {
        $get_posts = $index->search('', [
            'attributesToRetrieve' => null,
            'attributesToHighlight' => null,
            'hitsPerPage' => 1000,
            'analytics' => false
        ]);
    }
    return number_format($get_posts['nbHits'], 0, '', ',');
}

function truncateTitle($text, $limit=20){
    // temp solution to limit title length
    if (strlen(esc_attr( $text )) > $limit) {
        $str = substr(esc_attr( $text ), 0, $limit);
        if ($str!=$text){
            $str = $str . '...';
        } else {
            $str = $text;
        }
    } else {
        $str = esc_attr( $text );
    }
    return $str;
} 

function removeAllSlashes($string){
    $string=implode("",explode("\\",$string));
    return stripslashes(trim($string));
}

function keyword_count_sort($first, $sec) {
    return $sec[1] - $first[1];
}

function get_keywords($string, $min_word_length = 3, $min_word_occurrence = 0, $as_array = true, $max_words = 999) {

    $string = strip_tags($string);
    $string = iconv('UTF-8', 'ASCII//TRANSLIT', $string);     
    $string = preg_replace('/[^\p{L}0-9 ]/', ' ', $string);
    $string = trim(preg_replace('/\s+/', ' ', $string));

    $string = preg_replace('/[\x00-\x1F\x7F\xA0]/u', '', $string);

    $string = preg_replace('/\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', $string);

    $words = explode(' ', $string);

    $commonWords = array('a','able','about','above','abroad','according','accordingly','across','actually','adj','after','afterwards','again','against','ago','ahead','ain\'t','all','allow','allows','almost','alone','along','alongside','already','also','although','always','am','amid','amidst','among','amongst','an','and','another','any','anybody','anyhow','anyone','anything','anyway','anyways','anywhere','apart','appear','appreciate','appropriate','are','aren\'t','around','as','a\'s','aside','ask','asking','associated','at','available','away','awfully','b','back','backward','backwards','be','became','because','become','becomes','becoming','been','before','beforehand','begin','behind','being','believe','below','beside','besides','best','better','between','beyond','both','brief','but','by','c','came','can','cannot','cant','can\'t','caption','cause','causes','certain','certainly','changes','clearly','c\'mon','co','co.','com','come','comes','concerning','consequently','consider','considering','contain','containing','contains','corresponding','could','couldn\'t','course','c\'s','currently','d','dare','daren\'t','definitely','described','despite','did','didn\'t','different','directly','do','does','doesn\'t','doing','done','don\'t','down','downwards','during','e','each','edu','eg','eight','eighty','either','else','elsewhere','end','ending','enough','entirely','especially','et','etc','even','ever','evermore','every','everybody','everyone','everything','everywhere','ex','exactly','example','except','f','fairly','far','farther','few','fewer','fifth','first','five','followed','following','follows','for','forever','former','formerly','forth','forward','found','four','from','further','furthermore','g','get','gets','getting','given','gives','go','goes','going','gone','got','gotten','greetings','h','had','hadn\'t','half','happens','hardly','has','hasn\'t','have','haven\'t','having','he','he\'d','he\'ll','hello','help','hence','her','here','hereafter','hereby','herein','here\'s','hereupon','hers','herself','he\'s','hi','him','himself','his','hither','home','hopefully','how','howbeit','however','hundred','i','i\'d','ie','if','ignored','i\'ll','i\'m','immediate','in','inasmuch','inc','inc.','indeed','indicate','indicated','indicates','inner','inside','insofar','instead','into','inward','is','isn\'t','it','it\'d','it\'ll','its','it\'s','itself','i\'ve','j','just','k','keep','keeps','kept','know','known','knows','l','last','lately','later','latter','latterly','least','less','lest','let','let\'s','like','liked','likely','likewise','little','look','looking','looks','low','lower','ltd','m','made','mainly','make','makes','many','may','maybe','mayn\'t','me','mean','meantime','meanwhile','merely','might','mightn\'t','mine','minus','miss','more','moreover','most','mostly','mr','mrs','much','must','mustn\'t','my','myself','n','name','namely','nd','near','nearly','necessary','need','needn\'t','needs','neither','never','neverf','neverless','nevertheless','new','next','nine','ninety','no','nobody','non','none','nonetheless','noone','no-one','nor','normally','not','nothing','notwithstanding','novel','now','nowhere','o','obviously','of','off','often','oh','ok','okay','old','on','once','one','ones','one\'s','only','onto','opposite','or','other','others','otherwise','ought','oughtn\'t','our','ours','ourselves','out','outside','over','overall','own','p','particular','particularly','past','per','perhaps','placed','please','plus','possible','presumably','probably','provided','provides','q','que','quite','qv','r','rather','rd','re','really','reasonably','recent','recently','regarding','regardless','regards','relatively','respectively','right','round','s','said','same','saw','say','saying','says','second','secondly','see','seeing','seem','seemed','seeming','seems','seen','self','selves','sensible','sent','serious','seriously','seven','several','shall','shan\'t','she','she\'d','she\'ll','she\'s','should','shouldn\'t','since','six','so','some','somebody','someday','somehow','someone','something','sometime','sometimes','somewhat','somewhere','soon','sorry','specified','specify','specifying','still','sub','such','sup','sure','t','take','taken','taking','tell','tends','th','than','thank','thanks','thanx','that','that\'ll','thats','that\'s','that\'ve','the','their','theirs','them','themselves','then','thence','there','thereafter','thereby','there\'d','therefore','therein','there\'ll','there\'re','theres','there\'s','thereupon','there\'ve','these','they','they\'d','they\'ll','they\'re','they\'ve','thing','things','think','third','thirty','this','thorough','thoroughly','those','though','three','through','throughout','thru','thus','till','to','together','too','took','toward','towards','tried','tries','truly','try','trying','t\'s','twice','two','u','un','under','underneath','undoing','unfortunately','unless','unlike','unlikely','until','unto','up','upon','upwards','us','use','used','useful','uses','using','usually','v','value','various','versus','very','via','viz','vs','w','want','wants','was','wasn\'t','way','we','we\'d','welcome','well','we\'ll','went','were','we\'re','weren\'t','we\'ve','what','whatever','what\'ll','what\'s','what\'ve','when','whence','whenever','where','whereafter','whereas','whereby','wherein','where\'s','whereupon','wherever','whether','which','whichever','while','whilst','whither','who','who\'d','whoever','whole','who\'ll','whom','whomever','who\'s','whose','why','will','willing','wish','with','within','without','wonder','won\'t','would','wouldn\'t','x','y','yes','yet','you','you\'d','you\'ll','your','you\'re','yours','yourself','yourselves','you\'ve','z','zero');

    $moreCommon = array(
        'excerpt',
        'EST', 'PST',
        'january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december',
        'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday',
        'http', 'https', 'URL',
    );
    
    $commonWords = array_merge($commonWords, $moreCommon);

    $words = array_udiff($words, $commonWords,'strcasecmp');

    $keywords = array();

    while(($c_word = array_shift($words)) !== null) {

        if (strlen($c_word) < $min_word_length) continue;
            $c_word = strtolower($c_word);
            if (array_key_exists($c_word, $keywords)) $keywords[$c_word][1]++;
            else $keywords[$c_word] = array($c_word, 1);
    }

    usort($keywords, 'keyword_count_sort');
    $final_keywords = array();

    foreach ($keywords as $keyword_det) {
        if ($keyword_det[1] < $min_word_occurrence) break;
            array_push($final_keywords, $keyword_det[0]);
    }

    $final_keywords = array_slice($final_keywords, 0, $max_words);

    return $as_array ? $final_keywords : implode(', ', $final_keywords);
}


?>